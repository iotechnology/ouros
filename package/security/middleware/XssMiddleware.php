<?php

namespace App\Security\Middleware;

class XssMiddleware extends Middleware
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {

        $response = $handler->handle($request);

        return $response;
    }

}
<?php

namespace App\Security\Middleware;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Configuration\Contract\ConfigRegisterInterface;

class CsrfMiddleware extends Middleware implements ConfigRegisterInterface
{

    private $expiration;

    private $excepts = [];

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {
        $this->config();
        $response = $handler->handle($request);
        
        if($this->isAccessibleMethod($request) || $this->isAccessibleRoute($request) || $this->tokenMatch($request))
        {
            return $response;
        }

        throw new \Exception(
            'Bad request due to Invalide Csrf Token',
            400
        );

    }

    protected function tokenMatch(ServerRequestInterface $request) : bool
    {

        $token = $this->getTokenFromRequest($request);

        if(Session::has('_token')){

            $sessionTokens = Session::token();

            if(isset($sessionTokens[hash_token($token)]))
            {
                $result = true;
                if(!Session::isValideToken(hash_token($token)))
                    $result =  false;

                Session::removeToken(hash_token($token));
                return $result;

            }

        }


        return false;

    }

    protected function getTokenFromRequest(ServerRequestInterface $request)
    {
        $token = null;
        if(isset($request->getParsedBody()['CSRF_TOKEN']))
            $token = $request->getParsedBody()['CSRF_TOKEN'];

        return $token;

    }

    /**
     * Check if CsrfToken is needed to check so if is not get, head, options http method
     *
     * @param ServerRequestInterface $request
     * @return boolean
     */
    protected function isAccessibleMethod(ServerRequestInterface $request) : bool
    {
        return in_array($request->getMethod(), ['GET', 'HEAD', 'OPTIONS']);
    }

    protected function isAccessibleRoute(ServerRequestInterface $request) : bool
    {

        $path = str_replace('url=','',$request->getUri()->getQuery());
        foreach($this->excepts as $test)
        {
            if(\preg_match($test, $path)) return true;
        }

        return false;
    }

    protected function isValideToken(ServerRequestInterface $request) : bool
    {

    }

    public function config($args = null)
    {

        $config = config.config_csrf;
        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );
        $result = require $config;
        
        foreach($result as $key => $value)
        {
            if($key == 'expiration') $this->expiration = $value;
            if($key == 'excepts') $this->excepts = $value + $this->excepts;
        }

    }

}
<?php
namespace App\Event\Hub;

use App\Event\Contract\ListenerInterface;

class Listener implements ListenerInterface
{

    private $callable;

    private $name;

    public function __construct(string $name ,$callable)
    {

        if(!\is_array($callable) && !\is_callable($callable))
            throw new \InvalidArgumentException(
                "Trying to create new Listener with invalid argument at callable argument",
                500
            );

        $this->callable = $callable;
        $this->name = $name;
    }

    public function register() : array
    {
        return [
            $this->name => [$this->callable , $this->getPriority()],
        ];
    }

    private function getPriority() : int
    {
        if(\is_array($this->callable) && isset($this->callable["priority"]))
            return $this->callable["priority"];
        return 0;
    }

}
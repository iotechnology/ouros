<?php

namespace App\Event\Hub;

use Psr\EventDispatcher\StoppableEventInterface;
use App\Event\Contract\EventInterface;

class Event implements StoppableEventInterface, EventInterface
{
    
    protected $name;

    protected $stopped;

    protected $active;

    protected $context;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->stopped = false;
        $this->active = true;

    }

    public function with(array $context)
    {
        $this->context = $context;
    }

    public function isPropagationStopped() : bool
    {
        return $this->stopped;
    }

    public function stopPropagation(bool $value)
    {
        $this->stopped = $value;
    }

    public function activate(bool $active)
    {
        $this->active = $active;
    }

    public function isActive() : bool
    {
        return $this->active;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function __invoke($name)
    {
        if(!isset($this->context[$name]))
            throw new \Exception(
                "Try to acces at unknow variable " . $name,
                500 
            );

        return $this->context[$name];
    }

    public function __get($name)
    {
        if(!isset($this->context[$name]))
            throw new \Exception(
                "Try to acces at unknow variable " . $name,
                500 
            );

        return $this->context[$name];
    }

}
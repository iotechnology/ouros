<?php

namespace App\Event\Hub;

use App\Event\Contract\ListenerProviderInterface;
use Psr\EventDispatcher\ListenerProviderInterface as PsrLP;
use App\Event\Contract\EventInterface;

class ListenerProvider implements PsrLP , ListenerProviderInterface
{

    protected $events = [];

    public function getEvents()
    {
        return $this->events;
    }

    public function addListenerTo(string $event, $callable)
    {
        
        if(!\is_callable($callable) && !$callable instanceof ListenerInterface)
            throw new InvalidArgumentException(
                "2 parameters must be an instance of ListenerInterface or callable",
                500
            );

        if(\is_callable($callable))
            $callable = new Listener($event, $callable);

        if(!\array_key_exists($event, $this->events))
            $this->events = array_merge( $this->events, [$event => [$callable]]);
        
        $this->events[$event][] = $callable;

    }

    public function getListenersForEvent(object $event) : iterable
    {
        
        if(is_string($event))
            $ev = $event;
        if($event instanceof EventInterface)
            $ev = $event->getName();

        if(!isset($this->events[$ev]))
            $this->events = array_merge($this->events, [ $ev => [] ]);

        return $this->events[$ev];
    }

}
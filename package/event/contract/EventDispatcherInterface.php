<?php
namespace App\Event\Contract;

use App\Event\Hub\ListenerProvider;

interface EventDispatcherInterface
{

    public function __construct(ListenerProvider $listenerProvider = null);

    public function on(string $name);

    public function off(string $name);

    public function link(string $name, callable $callable, array $options = []);

}
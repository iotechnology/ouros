<?php

namespace App\Event\Contract;

interface ListenerInterface
{

    public function register() : array;

}
<?php
namespace App\Event\Contract;

interface EventInterface
{


    public function __construct(string $name);

    public function with(array $context);

    public function activate(bool $active);

    public function isActive() : bool;

    public function getName() : string;


    

}
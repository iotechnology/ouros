<?php
namespace App\Event\Service;

use Psr\EventDispatcher\EventDispatcherInterface as PsrED;
use Psr\EventDispatcher\ListenerProviderInterface as PsrLP;
use App\Event\Contract\EventDispatcherInterface;
use App\Event\Contract\EventInterface;

class _eventmanager implements PsrED , EventDispatcherInterface
{

    private $listenerProvider;

    public function __construct(ListenerProvider $listenerProvider = null)
    {

        if(is_null($listenerProvider))
            $listenerProvider = new ListenerProvider();

        $this->listenerProvider = $listenerProvider;

    }

    public function on(string $name)
    {
        
    }

    public function off(string $name)
    {

    }

    public function link(string $name, $callable, array $options = [])
    {
        $this->listenerProvider->addListenerTo($name, $callable);
    }

    public function trigger(string $name, array $params)
    {
        $event = new Event($name);
        $event->with($params);
        $this->dispatch($event);
    }

    public function dispatch(object $event)
    {


        if(!$event instanceof EventInterface)
            throw new \InvalidArgumentException(
                "Event argument must be Event class, this is " . get_class($event),
                500
            );

        if(is_string($event))
            $ev = $event;
        if($event instanceof EventInterface)
            $ev = $event->getName();

        $iterable = $this->listenerProvider->getListenersForEvent($event);

        usort($iterable, function($l1, $l2) use ($ev)
        {
            if(is_array($l1->register()[$ev]) && isset($l1->register()[$ev]['priority']))
                $v1 = $l1->register()[$ev]['priority'];
            else
                $v1 = 0;
            if(is_array($l2->register()[$ev]) && isset($l2->register()[$ev]['priority']))
                $v2 = $l2->register()[$ev]['priority'];
            else
                $v2 = 0;

            return $v1 - $v2;
        });

        if($event->isActive())
        foreach($iterable as $listener)
        {
            if($event->isStopped()) break;
            $callable = $listener->register()[$ev];
            if(\is_array($callable))
                $callable = $callable[0];
            if(!$listener instanceof Listener)
                $callable = [$listener, $callable];

            \call_user_func_array($callable, [$event]);

        }

    }


}
<?php

namespace App\Utility\FileSystem\Hub;

use App\Utility\FileSystem\Contract\FileSystemInterface;

use \FilesystemIterator;
use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;
use \Iterator;
use \CallbackFilterIterator;
use \RegexIterator;
USE \SplFileInfo;

class FileSystem implements FileSystemInterface
{

    private $defaultFlags = FilesystemIterator::CURRENT_AS_FILEINFO 
                            | FilesystemIterator::KEY_AS_PATHNAME 
                            | FilesystemIterator::SKIP_DOTS;

    public function find(string $path, $filter = null , ?int $flags = null)
    {

        $flags = $flags ?? $this->defaultFlags;

        $iterator = new FilesystemIterator($path, $flags);

        if(is_null($filter))
            return $iterator;

        return $this->filter($filter, $iterator);

    }

    public function findRecursive(string $path, $filter = null , ?int $flags = null)
    {

        $flags = $flags ?? $this->defaultFlags;

        $iterator = new RecursiveDirectoryIterator($path, $flags);

        $recursiveIterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);

        if(\is_null($filter))
            return $recursiveIterator;

        return $this->filter($filter, $recursiveIterator);

    }

    public function filter($filter, Iterator $iterator)
    {
        if(is_string($filter)){
            return new RegexIterator($iterator, $filter);
        }

        return new CallbackFilterIterator($iterator, $filter);

    }

    public function copyDir(string $src, string $dest) : bool
    {
        $dest = (new SplFileInfo($dest))->getPathname();

        if(!\is_dir($dest))
            $this->createDir($dest);

        $iterator = $this->find($src);

        $result = true;
        foreach($iterator as $fileInfo)
        {
            $tmp = $dest. DIRECTORY_SEPARATOR . $fileInfo->getFilename();
            if($fileInfo->getType() === 'dir')
                $result = $this->copyDir($fileInfo->getPathName(), $tmp);
            else
                $result = @copy($fileInfo->getPathName(), $tmp);
        }

        return $result;

    }

    public function deleteDir(string $path) : bool
    {

        if(!\is_dir($path))
            return true;
        
        $iterator = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS),
                    RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach($iterator as $fileInfo)
        {
            if($fileInfo->getType() === 'dir')
                $this->deleteDir($fileInfo->getPathName());
            else
                @rmdir($fileInfo->getPathName());
        }

        if(@rmdir($path) === false)
            return false;

        return true;
    }

    public function createDir(string $path, int $mode = 0755) : bool
    {

        if(is_dir($path))
            return true;

        $mask = \umask(0);
        if(mkdir($path, $mode, true) === false)
        {
            \umask($mask);
            throw new \Exception(
                "Failed to create directory ".$path, 500
            );
        }

        \umask($mask);
        return true;

    }

    public function putContents(string $path, $contents, $flags = null)
    {

        $flags = $flags ?? LOCK_EX;

        $path = (new SplFileInfo($path));

        $ready = true;
        $result = null;
        if(!is_dir($path->getPath()))
            $ready = $this->createDir($path->getPath(), $chmod);
        
        if($ready)
            $result = \file_put_contents($path, $contents, $flags);
        
        
        return $result; 


    }

    public function getContents(string $path, $context = null , int $offset = 1, ?int $maxLen = null)
    {
        if(!\file_exists($path))
            return null;

        if(!is_null($maxLen))
            return \file_get_contents($path, false, $context, $offset, $maxLen);

        return \file_get_contents($path, false, $context, $offset);
    }


    public function touch(string $path, int $chmod = 0755, $time = null, $atime = null) : bool
    {

        $path = (new SplFileInfo($path));

        $ready = true;

        if(!is_dir($path->getPath()))
            $ready = $this->createDir($path->getPath(), $chmod);

        if(!$ready)
            return false;

        $mask = umask(0);

        $result = touch($path, $time, $atime);
        \chmod($path, $chmod);
        \umask($mask);

        return $result;

    }

}
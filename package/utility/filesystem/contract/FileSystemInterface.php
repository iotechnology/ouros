<?php

namespace App\Utility\FileSystem\Contract;

interface FileSystemInterface
{

    public function find(string $path, $filter = null , ?int $flags = null);

    public function findRecursive(string $path, $filter = null , ?int $flags = null);

    public function copyDir(string $src, string $dest) : bool;

    public function deleteDir(string $path) : bool;

    public function createDir(string $path, int $mode = 0755) : bool;

    public function putContents(string $path, $contents);
    
    public function getContents(string $path, $context = null ,int $offset = 0, ?int $maxLen = null);

    public function touch(string $path, int $chmod = 0755 ,$time = null, $atime = null ) : bool;

}
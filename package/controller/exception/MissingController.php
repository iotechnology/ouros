<?php

namespace App\Controller\Exception;

class MissingController extends \Exception
{

    public function __construct($message, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
    
}

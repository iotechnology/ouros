<?php

namespace App\Controller\Contract;

use ControllerInterface;
use \Closure;

interface ControllerLauncherInterface
{

    public function __construct(ControllerLoaderInterface $loader);

    public function action(string $action);

    public function callControllerWith(array $params);


}
<?php
namespace App\Controller\Contract;

interface ControllerLoaderInterface
{

    public function loadController(array $route);

}
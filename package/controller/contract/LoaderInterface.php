<?php

namespace App\Controller\Contract;

interface LoaderInterface
{

    public function loadClass($path , ?string $classname = null);

    public function getClassName() : string;

    public function getPath() : string;

    public function getPathName() : string;

    public function getFileName() : string;

    public function getResolvedName() : string;

    public function isResolved() : bool;


}
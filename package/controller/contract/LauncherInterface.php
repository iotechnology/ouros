<?php

namespace App\Controller\Contract;
use \ReflectionClass;

interface LauncherInterface
{

    public function constructWith(array $params);

    public function with(array $params);

    public function method(string $func);

    public function call(?string $func = null);

    public function callArray(?string $func = null, ?array $params);
    
    public function getReflectionClass() : ReflectionClass;

    public function getInstance() : object;

    public function getClassName() : string;
}
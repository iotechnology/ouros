<?php

namespace App\Controller\Contract;

interface ControllerInterface
{
    
    public function __construct($request, $response, array $data);

}
<?php


namespace App\Controller\Middleware;

use ReflectionClass;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ServerResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Controller\Hub\ControllerLoader;
use App\Controller\Hub\ControllerLauncher;

/**
 * Undocumented class
 */
class ControllerMiddleware extends Middleware
{


    /**
     * Undocumented function
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {

        /**
         * Call next middleware
         */
        $response = $handler->handle($request);

        $route = $request->getAttribute(uri);

        /**
         * Load Controller
         */
        EventManager::trigger('controller.before.load', ['request' => $request->getAttributes()]);
        $loader = new ControllerLoader();
        $loader->loadController($route);
        
        EventManager::trigger('controller.initialize', ['request' => $request->getAttributes()]);
        $ControllerLauncher = new ControllerLauncher($loader);
        EventManager::trigger('controller.after.load', ['request' => $request->getAttributes()]);


        EventManager::trigger('controller.before.launch', ['request' => $request->getAttributes()]);
        /**
         * Finally call controller
         */
        $response = $ControllerLauncher->constructWith(['request' => $request, 'response' => $response, 'data' => []])->action($route['path']['action'])->callControllerWith($route['matches']);
        EventManager::trigger('controller.after.launch', ['request' => $request->getAttributes()]);


        return $response;
    
    }

}

<?php

namespace App\Controller\Hub;

use App\Controller\Hub\Loader;
use App\Controller\Contract\LauncherInterface;
use App\Controller\Contract\LoaderInterface;
use \ReflectionClass;

class Launcher implements LauncherInterface
{

    /**
     * Fully class name
     *
     * @var string
     */
    protected $class;

    /**
     * data to send at the controller function
     *
     * @var array
     */
    protected $params;

    /**
     * data to send at the controller construtor
     *
     * @var [type]
     */
    protected $constructorparams;

    /**
     * function name
     *
     * @var string
     */
    protected $func;

    /**
     * Reflexion Class use to construct
     *
     * @var ReflectionClass
     */
    protected $object;

    /**
     * Instance of class
     *
     * @var mixed
     */
    protected $instance;
    

    public function __construct(LoaderInterface $loader)
    {

        $this->class = $loader->getResolvedName();

        if(!$loader->isResolved())
            throw new RuntimeException(
                "Loader must be resolved before using an launcher with this", 
                500
            );

        $this->object = new ReflectionClass($this->class);
    
    }


    public function constructWith(array $params)
    {

        if(!isset($this->object))
            throw new RuntimeException(
                "Neither class is defined",
                500
            );

        if(!empty($params))
            $this->constructorparams = $params;

        return $this;

    }

    /**
     * Add params before call
     *
     * @param array $params
     * @return void
     */
    public function with(array $params){

        if(!isset($params) || empty($params))
            throw new \RuntimeException(
                "Attempt to add parameters without parameters : " . $this->class,
                500 
            );

        $this->params = $params;

        return $this;

    }

    /**
     * Register method needed to call
     *
     * @param string $func
     * @return void
     */
    public function method(string $func)
    {

        if(!$this->object->hasMethod($func))
            throw new \RuntimeException(
                $func . "not exist on class " . $this->class,
                500 
            );

        $this->func = $func;

        return $this;
        
    }


    /**
     * Call method without parameter or with parameter already register with "with" method
     *
     * @param string $func
     * @return void
     */
    public function call(?string $func = null)
    {

        if(!is_null($func)) 
            $this->method($func);

        if(!isset($this->object) || !isset($this->func))
            throw new \RuntimeException(
                "Neither class or method is defined",
                500
            );

        
        $instance =  $this->construct();

        if(!isset($this->params)) 
            $this->params = [];

        \call_user_func_array( [$instance, $this->func] ,  $this->params );

    }

    /**
     * Call function with parameters
     *
     * @param string $func
     * @param array $params
     * @return void
     */
    public function callArray(?string $func = null, ?array $params)
    {

        if(!empty($params))
            $this->data = $params;
        if(!is_null($func)) 
            $this->method($func);
        if(!isset($this->object) || !isset($this->func) || !isset($this->params))
            throw new RuntimeException(
                "Neither method or function or params is defined",
                500
            );

        $instance = $this->construct();

        if(isset($this->params))
            \call_user_func_array( [$instance, $this->func] ,  $this->params );


    }

    /**
     * Contruct new instance of class
     *
     * @return void
     */
    private function construct()
    {

        if(isset($this->constructorparams) && !empty($this->constructorparams))
            return $this->object->newInstanceArgs($this->constructorparams);

        return $this->object->newInstance();

    }

    /**
     * Retrieve Reflection class
     *
     * @return void
     */
    public function getReflectionClass() : ReflectionClass
    {

        return $this->object;

    }

    /**
     * Retrieve an instance of class
     *
     * @return void
     */
    public function getInstance() : object
    {

        return $this->instance;
    
    }

    /**
     * Retrieve class name
     *
     * @return void
     */
    public function getClassName() : string
    {

        return $this->class;
    
    }

}
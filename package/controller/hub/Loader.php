<?php

namespace App\Controller\Hub;

use App\Controller\Contract\LoaderInterface;
use App\Utility\FileSystem\Hub\FileSystem;

use App\Controller\Exception\MissingFile;
use App\Controller\Exception\MissingFolder;

class Loader implements LoaderInterface
{

    /**
     * path without filename
     *
     * @var string
     */
    protected $path;

    /**
     * filename
     *
     * @var string
     */
    protected $filename;

    /**
     * path with filename
     *
     * @var string
     */
    protected $pathname;

    /**
     * class name
     *
     * @var string
     */
    protected $classname;

    /**
     * All namespace containing on the file
     *
     * @var array
     */
    protected $namespaces = [];

    /**
     * Full name of class
     *
     * @var string
     */
    protected $resolvedName;

    /**
     * SplFileInfo
     *
     * @var SplFileInfo
     */
    protected $info;

    /**
     * is resolved
     *
     * @var boolean
     */
    protected $resolved = false;

    /**
     * Constructor
     *
     * @param string/array/null $path
     * @param string|null $classname
     */
    public function __construct($path = null , ?string $classname = null)
    {

        if(!is_null($path))
            $this->loadClass($path, $classname);
    }

    /**
     * Load all information needed to create new instance
     * !!! this function not create new instance of class just load class
     * 
     * @param string/array $path
     * @param string|null $className
     * @return void
     */
    public function loadClass($path , ?string $classname = null)
    {
        $this->resolved = false;
        if(!\is_string($path) && !\is_array($path))
            throw new \InvalideArgumentException(
                __METHOD__ . " argument must be an array or a string.", 500
            );          

        $this->parseFile($path);
        $this->loadFile();
        
        $this->parseNamespaces();

        if(is_null($this->classname))
            $this->classname = $this->parseClassName();

        $this->classname = $this->classname;

        if(!empty($this->namespaces))
        {
            foreach($this->namespaces as $namespace)
            {

                if(class_exists($namespace."\\".$this->classname))
                {
                    $this->resolvedName = $namespace."\\".$this->classname;
                    break;
                }

            }
        }
        else
        {
            $this->resolvedName = $this->classname;
        }

        if(!class_exists($this->resolvedName))
        {
            throw new \MissingFile(
                $className . " class not found", 500
            );
        }

        $this->resolved = true;

    }

    /**
     * Require file using common path and filename
     *
     * @param array $path
     * @return void
     */
    private function loadFile()
    {

        if(isset($this->path) && !is_null($this->path) && isset($this->filename) && !is_null($this->filename))
            require_once $this->path.'/'.$this->filename;
        else
            throw new \Exception(
                "Cannot use loadFile if path and filename is null or not create. Use parseFile before !",
                500
            );

    }

    /**
     * Check existance of file and return this if is the only result.
     *
     * @param string $path
     * @return boolean
     */
    private function checkFile($iterators, string $filename, string $file)
    {

        if(!is_null($iterators->next()))
            throw new \MissingFile(
                "Can't resolve find of class file because found multiple file with this name : " . $file . " on folder " . $filename,
                500
            );

        $iterators->rewind();

        $iterator = $iterators->current();

        if(is_null($iterator))
            throw new \MissingFile(
                "Can't resolve find of class file because not found file with this name : " . $file . " on folder " . $filename, 
                500
            );

        return $iterator;

    }

    /**
     * Retrive the File and extract File Information
     *
     * @param string $filename
     * @param string $file
     * @return void
     */
    private function getFileInfo(string $filename, string $file)
    {
        
        $fileSystem = new FileSystem();
        $iterators = $fileSystem->find($filename, function($iterator) use($file){
            if($iterator->getFilename() == $file) return $iterator;
        });

        return $this->checkFile($iterators, $filename, $file);

    }

    /**
     * Deduce all information of file using path like array or string
     *
     * @param array/string $path
     * @return void
     */
    private function parseFile($path)
    {
        /**
         * Initialize variable
         */
        if(\is_array($path))
        {
            $tmp = $path;
        }
        if(\is_string($path))
        {
            $tmp = \explode('/', $path);
        }

        /**
         * Create new directory path on string
         * And extract filename for traitment
         */
        $pathname = root;
        if(\preg_match("#.php#", end($tmp)) === 0)
            $file = end($tmp).'.php';
        else
            $file = end($tmp);
        \array_pop($tmp);
        $pathname .= \implode('/', $tmp);
        
        /**
         * Checking existance of file and get new SplInfo
         */
        $iterator = $this->getFileInfo($pathname, $file);
        $this->filename = $iterator->getFilename();
        $this->pathname = $iterator->getPathName();
        $this->path     = $iterator->getPath();
        $this->info     = $iterator;

        return $iterator;
    }

    /**
     * Retrieve the class name using the file name 
     *
     * @return string
     */
    private function parseClassName() : string
    {

        return \preg_replace("#.php#",'',$this->filename);

    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getClassName() : string
    {
        return $this->classname;
    }

    /**
     * Get path without file name
     *
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    /**
     * Get path with file name
     *
     * @return string
     */
    public function getPathName() : string
    {
        return $this->pathname;
    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getFileName() : string
    {
        return $this->filename;
    }

    /**
     * Get full class name
     *
     * @return string
     */
    public function getResolvedName() : string
    {
        return $this->resolvedName;
    }

    /**
     * Get the statu of the loading of class
     *
     * @return boolean
     */
    public function isResolved() : bool
    {
        return $this->resolved;
    }

    /**
     * Retrieve all namespace on the file
     * !!! only use when parseFile and loadFile are exit successfully
     * @return void
     */
    private function parseNamespaces()
    {

        $fileSystem = new FileSystem();
        $content = $fileSystem->getContents($this->pathname);
        $matches = [];
        \preg_match_all("#namespace (.*);#",$content, $matches);

        $this->namespaces = [];

        foreach($matches[1] as $namespace)
        {

            $this->namespaces[] = $namespace;
        
        }

    }


}
<?php

namespace App\Controller\Hub;

use App\Controller\Contract\ControllerLauncherInterface;
use App\Controller\COntract\ControllerLoaderInterface;

use App\Controller\Exception\MissingAction;

class ControllerLauncher extends Launcher implements ControllerLauncherInterface
{

    protected $loader;

    public function __construct(ControllerLoaderInterface $loader)
    {
        /*$this->loader = $loader;
        $loaderFactory = new Loader();
        $loaderFactory->loadClass("package/factory/factories/ControllerFactory.php");*/

        parent::__construct($loader);

    }

    public function action(string $action)
    {
        try
        {
            $this->method($action);
        }
        catch(\Exception $e)
        {
            throw new MissingAction(
                $action . " action not found on class : " . $this->class,
                404
            );
        }

        return $this;

    }

    public function callControllerWith($params)
    {

        if(!empty($params))
        {
            if(!is_array($params))
                $params = [$params];

            $this->with($params);
        }

        if(!$this->accesibleCall())
            throw new \Exception(
                'Page not found',
                404
            );
        
            
        $response = $this->call();

        if(!isset($response) || \is_null($response))
            $response = $this->constructorparams['response'];
        return $response;
    }

    public function accesibleCall()
    {

        switch($this->func)
        {

            case 'beforeConstruct':
                return false;
            case 'afterConstruct';
                return false;
            case 'beforeCall':
                return false;
            case 'afterCall':
                return false;
            default:
                return true;


        }


    }


   


}
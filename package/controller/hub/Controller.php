<?php

namespace App\Controller\Hub;

use App\Controller\Contract\ControllerInterface;

abstract class Controller implements ControllerInterface
{

    protected $data;
    
    public function __construct($request, $response, array $data)
    {
        $this->beforeConstruct();
        $this->data = [];
        $this->data['request'] = $request;
        $this->data['response'] = $response;
        array_merge($this->data, $data);
        $this->afterConstruct();
    }




    public function __get($name)
    {
        
        if(!isset($this->data[$name]))
            throw new \Exception(
                $name . " variable not existe on controller",
                500
            );

        return $this->data[$name];

    }

    public function beforeConstruct()
    {

    }

    public function afterConstruct()
    {

    }

    public function beforeCall()
    {

    }

    public function afterCall()
    {

    }

    public function beforeRender()
    {

    }

}
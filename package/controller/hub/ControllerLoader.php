<?php

namespace App\Controller\Hub;

use App\Controller\Contract\ControllerLoaderInterface;
use App\Controller\Hub\Loader;

use App\Controller\Exception\MissingFile;
use App\Controller\Exception\MissingFolder;
use App\Controller\Exception\MissingController;
use App\Controller\Exception\MissingPrefix;

class ControllerLoader extends Loader implements ControllerLoaderInterface
{

    public function loadController(array $route)
    {

        $path = $this->parseRoute($route);

        try
        {

            $this->loadClass($path);

        }
        catch(\Exception $e)
        {
            
            if($e instanceof MissingFile)
                throw new MissingController(
                    $this->class . "Controller not found",
                    404,
                    $e
                );
            if($e instanceof MissingFolder)
                throw new MissingPrefix(
                    $this->class . "Controller folder not found",
                    404,
                    $e
                );
        }



    }

    private function parseRoute(array $route)
    {

        $tmp = $route['path'];
        $path = [];

        $path[] = 'src';
        $path[] = 'controller';
        
        isset($tmp['prefix']) && $tmp['prefix'] != 'default' ? $path[] = $tmp['prefix'] : $path;
        $path[] = $tmp['controller']."Controller";

        return $path;

    }

}

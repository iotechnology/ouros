<?php

/**
 * Application name
 */
define('app_name', 'ouros', true);

/**
 * Default base url change for prod env like www.ouros.com/
 */
define('app_base_url', 'ouros', true);

/**
 * Define current project root
 */
define('root', getcwd()."/", true);

/**
 * Define current config path
 */
define('config', './config/', true);

/**
 * Define name of application
 */
define('project_name', '/ouros', true);

/**
 * Define isDevMode
 */
define('dev_mode', true, true);

/**
 * Default salt use to create hash password
 */
define('default_salt_password','my_saltPas5w0rd', true);

/**
 * Define if we use https or http
 */
define('secure_url', false, true);

/**
 * definition of differente attribute's name for request
 * 
 * ? uri                -> use to get request attribute containing array of information to load Controller - Action - View
 * ? url                -> use to get request attribute containing current url
 * 
 * ! exception_code     -> use to get http code of error
 * ! uri_error          -> use to get request attribute containing array of information to load Controller - Action - View for error
 * 
 */

define('uri', 'uri', true);
define('url', 'url', true);
define('uri_error', 'uri_error', true);
define('exception_code', 'exception_code', true);

/**
 * Definition of configuration file use by class implements ConfigRegisterInterface
 */

define('config_debug', 'config.debug.php', true);
define('config_database', 'config.database.php', true);
define('config_middlewares', 'config.middlewares.php', true);
define('config_security', 'config.security.php', true);
define('config_session', 'config.session.php', true);
define('config_auth', 'config.auth.php', true);
define('config_csrf', 'config.csrf.php', true);

setlocale(LC_TIME, "fr_FR");
<?php

namespace App\Core\Autoload;

use App\Core\Exception\NotFoundException;
use App\Core\Contract\AutoloadInterface;

/**
 * Autoloading
 */
class Autoload implements AutoloadInterface
{

    /**
     * current iod container
     *
     * @var App\Iod\Service\Iod
     */
    private $app;

    public function __construct(\App\Core\Service\Iod $app){

        $this->app = $app;

    }

    /**
     * Autoloader
     *
     * @return void
     */
    public function autoloader($class)
    {

        $newClass = $this->getRealName($class);
        if(\is_string($newClass)){
            $path = $this->convertToFilePath($newClass);

            if($this->file_exists($path) ){
    
                require_once $path;
                class_alias($newClass,$class);

            }
        }

    }

    /**
     * Resolve real name of alias
     *
     * @param string $class
     * @return string
     */
    protected function getRealName(string $class)
    {
        
        $newClass = \explode('\\',$class);

        if(!is_null($newClass = $this->app->getResolveAlias(end($newClass))))
            return $newClass;
        
            

    
    }

    /**
     * Convert class name to path
     *
     * @param string $class
     * @return string
     */
    protected function convertToFilePath(string $class){
        
        $explode = \explode('\\',$class);
        $notNullIndex = 0;

        $explode[0] == "" ? array_shift($explode) : $notNullIndex;
        $explode[0] == "App" ? array_shift($explode) : $explode[0]; 
        array_unshift($explode, "package");


        $i = 0;
        while(isset($explode[$i]) && isset($explode[$i+1])){
            $explode[$i] = \lcfirst($explode[$i]);
            $i++;
        }

        $file = implode("/", $explode).".php";
        
        return root.$file;

    }

    /**
     * Check the existence file
     *
     * @return void
     */
    protected function file_exists($path)
    {
        return \file_exists($path);
    }

    /**
     * Register this autoload
     *
     * @return void
     */
    public function register()
    {

        \spl_autoload_register([$this, 'autoloader']);

    }



}
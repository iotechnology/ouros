<?php

namespace App\Core\Hub;

use App\Core\Exception\NotFoundException;
use App\Core\Exception\NotExistClassException;
use \Closure;
use \ReflectionClass;
use \ReflectionParameter;

/**
 * Class for dependancie injection
 */
class Container implements \ArrayAccess{

    /**
     * Array of all alias => concrete name
     *
     * @var array
     */
    protected $aliases = [];

    /**
     * Reverse aliases
     *
     * @var array
     */
    protected $abstractAliases = [];

    /**
     * All instances create after binding / make
     *
     * @var array
     */
    protected $instances = [];
    
    /**
     * Common binding
     *
     * @var array
     */
    protected $bindings = [];

    /**
     * Array of all resolved buindings
     *
     * @var array
     */
    protected $resolved = [];

    /**
     * Create new singleton
     *
     * @param string $abstract
     * @return void
     */
    public function singleton($abstract, $concrete = null)
    {

        $this->bind($abstract,$concrete, true);

    }

    /**
     * Resolve an entry
     *
     * @param string $abstract
     * @param array $parameters
     * @return void
     */
    protected function resolve($abstract, array $parameters = [])
    {

        if(isset($this->instances[$abstract]) && !$this->isRenewable($abstract)){
            return $this->instances[$abstract];
        }

        $concrete = $this->getConcrete($abstract);

        if($concrete === $abstract || $concrete instanceof Closure){
                $result = $this->build($concrete, $parameters);
        }else{
            $result = $this->make($concrete, $parameters);
        }

        if($this->isShared($abstract)){

            $this->instances[$abstract] = $result;

        }

        $this->resolved[$abstract] = true;

        return $result;
    }

    /**
     * Get concrete name stock in the container by abstract name
     *
     * @param string $abstract
     * @return void
     */
    public function getConcrete($abstract)
    {

        if(isset($this->bindings[$abstract]) && isset($this->bindings[$abstract]['concrete'])){
            return $this->bindings[$abstract]['concrete'];
        }

        return $abstract;

    }

    /**
     * Check if the binding is singleton
     *
     * @param string $abstract
     * @return boolean
     */
    public function isShared($abstract)
    {

        if(isset($this->bindings[$abstract]) && isset($this->bindings[$abstract]['shared'])){
            return $this->bindings[$abstract]['shared'];
        }
        return false;

    }

    /**
     * Check is the binding can be rewrite
     *
     * @param string $abstract
     * @return boolean
     */
    public function isRenewable($abstract)
    {

        if(isset($this->bindings[$abstract]) && isset($this->bindings[$abstract]['renewable'])){
            return $this->bindings[$abstract]['renewable'];
        }
        return false;
    }

    /**
     * Bind closure
     *
     * @param string/Closure $abstract
     * @param string/Closure $concrete
     * @param boolean $shared
     * @return void
     */
    public function bind($abstract, $concrete = null, $shared = false, $renewable = true)
    {
  
        if(is_null($concrete)){
            $concrete = $abstract;
        }

        if(!$concrete instanceof Closure){
            $concrete = $this->createClosure($abstract, $concrete);
        }
        
        $this->bindings[$abstract] = compact('concrete', 'shared', 'renewable');

    }

    /**
     * Create new Closure with abstract name and concrete name 
     *
     * @param string $abstract
     * @param string $concrete
     * @return void
     */
    protected function createClosure($abstract, $concrete)
    {

        return function ($parameters = []) use ($abstract, $concrete) {
            $method = ($abstract === $concrete) ? 'build' : 'make';
            return $this->$method($concrete, $parameters);
        };

    }

    /**
     * create new instance if not make of the binding
     *
     * @param string $concrete
     * @param array $parameters
     * @return void
     */
    public function build($concrete, $parameters = [])
    {

        if($concrete instanceof Closure){
            return $concrete($parameters);
        }

        if(class_exists($concrete)){
            
            return $this->resolveClass($concrete, $parameters);

        }else{

            throw new NotExistClassException();
        
        }

    }

    /**
     * Resolve instanciation of class
     *
     * @param string $concrete
     * @param array $parameter
     * @return void
     */
    protected function resolveClass($concrete, $parameters)
    {

        $reflection = new ReflectionClass($concrete);
        $constructor = $reflection->getConstructor();
        
        if(is_null($constructor)){
            
            return new $concrete();
        }else{

            $resolveDep = $this->resolveDependencies($constructor, $parameters);

            return $reflection->newInstanceArgs($resolveDep);
        }

    }


    /**
     * Resolve all dependencies needed to construct an object
     *
     * @param [type] $constructor
     * @param [type] $parameters
     * @return void
     */
    protected function resolveDependencies($constructor, $parameters)
    {

        $dependencies = $constructor->getParameters();
        $resolveDep = [];
        
        foreach($dependencies as $d){
            if(\array_key_exists($d->name,$parameters)){
                $resolveDep[] = $parameters[$d->name];

            }else{

                if($d->getClass() === get_class($this)) 
                {
                    $resolveDep[] = $this;
                }
                else
                {
                    $resolveDep[] = is_null($d->getClass()) 
                    ? $this->resolveParameterPrimitve($d) 
                    : $this->resolveParameterClass($d);
                }
       
            }
        }

        return $resolveDep;
    }


    /**
     * Resolve parameter of class where parameter is an primitive
     *
     * @param ReflectionParameter $parameter
     * @return void
     */
    protected function resolveParameterPrimitve(ReflectionParameter $parameter)
    {

        if($parameter->isOptional()){
            return $parameter->getDefaultValue();
        }

    }

    /**
     * Resolve parameter of class where parameter is an class
     *
     * @param ReflexionParameter $parameters
     * @return void
     */
    protected function resolveParameterClass(ReflectionParameter $parameter)
    {

        try{

            return $this->make($parameter->getClass()->name);

        }catch(\Exception $e){

            if($parameter->isOptional()){
                return $parameter->getDefaultValue();
            }

            throw $e;

        }

    }

    /**
     * Create new instance binding to abstract
     *
     * @param string $abstract
     * @return void
     */
    public function make($abstract, $parameters = [])
    {
        if(!\is_array($parameters)){
            $parameters = [$parameters];
        }
        
        return $this->resolve($abstract, $parameters);
    }

    /**
     * If instance exist return this else create new instance for abstract
     *
     * @param string $abstract
     * @param array $parameters
     * @return void
     */
    public function get($abstract, $parameters = [])
    {

        if(isset($this->instances[$abstract])){

            return $this->instances[$abstract];
        }

        return $this->make($abstract, $parameters);

    }

    /**
     * Check if $id exist on the container
     *
     * @param string $id
     * @return boolean
     */
    public function has($id)
    {

        return isset($this->bindings[$id]) || 
        isset($this->instances[$id]) ||
        $this->hasAlias($id);
    
    }

    /**
     * Resolve Alias
     *
     * @param string $abstract
     * @return void
     */
    public function getResolveAlias($abstract)
    {

        if($this->hasAlias($abstract)){
            if(isset($this->aliases[$abstract]))
                return $this->aliases[$abstract];
            

            if(isset($this->abstractAliases[$abstract]))
                return $abstract;
        }

        return null;

    }

    /**
     * Found Alias name on container
     *
     * @param string $concrete
     * @return void
     */
    public function getAlias($concrete)
    {

        if($this->hasAlias($concrete)){
            if(isset($this->aliases[$concrete]))
                return $concrete;
            

            if(isset($this->abstractAliases[$concrete]))
                return $this->abstractAliases[$concrete];
        }

        return null;

    }

    /**
     *Check if alias exist on the container
     *
     * @param string $abstract
     * @return boolean
     */
    public function hasAlias($abstract)
    {

        return \array_key_exists($abstract, $this->aliases) || array_key_exists($abstract, $this->abstractAliases);

    }

    /**
     * Set an alias
     *
     * @param string $abstract
     * @param string $concrete
     * @return void
     */
    public function alias($abstract, $concrete = null)
    {
        if(\is_null($concrete))
        {
            $concrete = $abstract;
        }

        $this->abstractAliases[$concrete] = $abstract;
        $this->aliases[$abstract] = $concrete;
    }

    /**
     * Clear all containers
     *
     * @return void
     */
    public function clear()
    {

        $this->instances = [];
        $this->bindings = [];
        $this->aliases = [];
        $this->resolved = [];

    }

    /**
     * Clear all instance stack in this container
     *
     * @return void
     */
    public function clearInstances()
    {

        $this->instances = [];

    }

    /**
     * Alias to clear method
     *
     * @return void
     */
    public function reset()
    {

        $this->clear();

    }

    /**
     * Check if $offset exist on this container
     *
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists ( $offset )
    {

        return $this->has($offset);

    }

    /**
     * Resolve $offset and return the result
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet ( $offset ) 
    {

        return $this->get($offset);

    }

    /**
     * Create new bind
     *
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet ( $offset ,  $value ) 
    {

        $this->bind($offset, $value);

    }

    /**
     * Remove an $offset on this container
     *
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset ( $offset )
    {

        unset($this->instances[$offset], $this->bindings[$offset], $this->resolved[$offset]);
    
    }

    /**
     * Get registry $input
     *
     * @param string $offset
     * @return mixed
     */
    public function __invoke( $input )
    {

        return $this->get($input);

    }

}
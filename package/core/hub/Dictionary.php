<?php

namespace App\Core\Hub;

class Dictionary
{

    private static $instance;

    public $data = [

        'DIRECTORY'             => "/",
        'ROOT'                  => root,
        'SRC'                   => "%ROOT%/src/",
        'CONTROLLER'            => "%SRC%/controller/",
        'ERROR_CONTROLLER'      => "%CONTROLLER%/error/",
        'MODEL'                 => "%ROOT%/model/",
        'ENTITY'                => "%MODEL%/entity/",
        'TEMPLATE'              => "%SRC%/template/",
        'LAYOUT'                => "%TEMPLATE%/layout/",
        'VIEW'                  => "%TEMPLATE%/view/",
        'WEBROOT'               => "%ROOT%/webroot/",
        'CSS'                   => "%WEBROOT%/css/",
        'JS'                    => "%WEBROOT%/js/",
        'IMG'                   => "%WEBROOT%/img/",
        'PACK'                  => "%ROOT%/package/",
        'CONFIG'                => "%ROOT%/config/",

    ];

    public function __construct(){
        if(!isset(static::$instance) || empty(static::$instance) || is_null(static::$instance)){
            static::$instance = $this;
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @param [type] $value
     */
    public function __set($name, $value){
        if(array_key_exists($name, $this->data)){
            $this->data[$name] = $value;
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @return void
     */
    public function __get($name){

        if(array_key_exists($name, $this->data)){
            return $this->resolvePath($this->data[$name]);
        }

        $trace = debug_backtrace();
        trigger_error(
            'Propriété non-définie via __get() : ' . $name .
            ' dans ' . $trace[0]['file'] .
            ' à la ligne ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function resolvePath($path) : string{

        preg_match("#%([A-Z]+)%/#",$path, $matches);

        if(isset($matches) && isset($matches[1])){
            $result = $this->resolvePath($this->data[$matches[1]]);
            $path = \str_replace($matches[0],$result,$path);
        }

        return $path;


    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @param [type] $arguments
     * @return void
     */
    public static function __callStatic($name , $arguments){

        if(!empty($arguments) && isset($arguments[0])){
            static::$instance->$name = $arguments[0];
        }
        return static::$instance->$name;

    }

}


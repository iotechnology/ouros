<?php

namespace App\Core\Hub;

/**
 * Blind class use to create common Blind use in app
 */
class Blind
{


    /**
     * All resolve Service Instances
     *
     * @var array
     */
    public static $resolveServices = [];

    /**
     * Init Blind with new instance of the bind service if not already create
     *
     * @return void
     */
    public static function init()
    {

        if(!(static::isResolveService())){
            static::resolveService();
        }

    }

    /**
     * Return instance of bind service
     *
     * @return void
     */
    public static function getService()
    {

        static::init();
        return static::$resolveServices[static::getAccessor()];

    }

    /**
     * return the name of bind service
     *
     * @return void
     */
    public static function getNameService()
    {

        static::init();
        return get_class(static::$resolveServices[static::getAccessor()]);

    }

    /**
     * Find service correspondind of this blind
     *
     * @return void
     */
    protected static function resolveService()
    { 
        static::$resolveServices[static::getAccessor()] = get(static::getAccessor());
    }

    /**
     * Return true if servie was already resolved
     *
     * @return boolean
     */
    protected static function isResolveService() : bool
    {

        return isset(static::$resolveServices[static::getAccessor()]);

    }
    /**
     * get Accessor Name
     *
     * @return string
     */
    protected static function getAccessor() : string 
    {

        if(!isset(static::$accessor)){
            $alias = get_alias(\get_called_class());
            return lcfirst($alias);
        }

        return static::$accessor;

    }

    /**
     * Call with methods with static sintax, using instance of class
     *
     * @param string $name
     * @param mixed $arguments
     * @return void
     */
    public static function __callStatic($name, $arguments)
    {

        static::init(); 
        $result = call_user_func_array([static::$resolveServices[static::getAccessor()], $name], $arguments);
        return $result;

    }

}

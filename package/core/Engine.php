<?php

/* *
* Init core of the application
* This is the file call by every request
* */

/**
 * Load environement configuration
 */
require_once 'package/definition.php';

/**
 * Load composer autoloader
 */
require_once './vendor/autoload.php';

/**
 * Load function helpers
 */
require_once 'package/helper.php';


/**
 * Create the first and last instance of iod container
 */
$iod = new App\Core\Service\Iod();

/**
 * Set all definitions files for the container
 */
$iod->setAliasFile("config/iod.alias.php");
$iod->setBindsFile("config/iod.binds.php");
$iod->setServicesFile("config/iod.services.php");

/**
 * Create new autoloader
 */
$autoloader = new \App\Core\Autoload\Autoload($iod);

/**
 * add this autoloader to the autoloading stack
 */
$autoloader->register();

/**
 * Load all services require in config\iod.alias.php
 */
$iod->aliases();

/**
 * Load all services require in config\iod.binds.php
 * !!! All bindings need to be instanciate later (is not singleton)
 */
$iod->binds();

/**
 * Load all services require in config\iod.services.php
 * !!! All services which instanciate with this method are singleton and can't be reeinstenciate.
 */
$iod->services();



/**
 * Configure App
 */
require_once "config/app.php";

/**
 * Get current http request
 */
$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();

if(isset($request->getQueryParams()[url]))
    $request = $request->withAttribute( url , $request->getQueryParams()[url]);

/**
 * Launch Dispatcher and get Response
 */
$response = Dispatcher::handle($request);

/**
 * Send response get by middlewares
 */
\Http\Response\send($response);
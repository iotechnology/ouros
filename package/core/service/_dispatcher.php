<?php

namespace App\Core\Service;

/**
 * Dispatch middlewares
 */
class _dispatcher extends MiddlewareManager 
{

    public function config($args = null)
    {

        $config = config.config_middlewares;
        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );
        
        
        $result = require_once $config;
        foreach($result as $key => $value)
        {
            
            if($key == 'middlewares') $this->puts('middlewares', $value);
            if($key == 'detach_middlewares') $this->puts('detach', $value);

        }

    }

}

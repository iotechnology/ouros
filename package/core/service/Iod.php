<?php

namespace App\Core\Service;

use App\Core\Hub\Container;

/**
 * Undocumented class
 */
class Iod extends Container{

    /**
     * current instance of the container
     *
     * @var Iod
     */
    public static $instance;

    /**
     * location of aliases definitions
     *
     * @var string
     */
    private $aliasFile;

    /**
     * location of bindings definitions
     *
     * @var string
     */
    private $bindsFile;

    /**
     * location of services definitions
     *
     * @var string
     */
    private $servicesFile;

    public function __construct(){

        static::$instance = $this;

    }

    /**
     * Load all alias need for loading class with autoload
     *
     * @return void
     */
    public function aliases()
    {

        $blinds = require_once $this->aliasFile;

        foreach($blinds as $abstract => $concrete){

            $this->alias($abstract, $concrete);
            
        }

    }

    /**
     * Load all common binding no singleton and can be rewrite
     *
     * @return void
     */
    public function binds()
    {

        $binds = require_once $this->bindsFile;

        foreach($binds as $abstract => $concrete){

            if(\is_array($concrete))
            {
                $shared = false;
                $renewable = true;
                if(isset($concrete[1])) $shared = $concrete[1];
                if(isset($concrete[2])) $renewable = $concrete[2];
                $this->bind($abstract, $concrete[0], $shared, $renewable);
            }
            else
            $this->bind($abstract, $concrete, false);

        }

    }

    /**
     * Load all services and common class need to instanciate like singleton and are not rewrite
     *
     * @return void
     */
    public function services()
    {

        $services = require_once $this->servicesFile;

        foreach($services as $abstract)
        {
            
            $this->make($abstract);

        }   

    }

    /**
     * Set location of aliases definitions
     *
     * !!! file must be return an array of definitions
     * 
     * @param string $file
     * @return void
     */
    public function setAliasFile(string $file)
    {

        $this->aliasFile = $file;

    }

    /**
     *  Set location of bindings definitions
     *
     *  !!! file must be return an array of definitions
     *
     * @param string $file
     * @return void
     */
    public function setBindsFile(string $file)
    {

        $this->bindsFile = $file;

    }

    /**
     *  Set location of services definitions
     *
     *  !!! file must be return an array of definitions
     *
     * @param string $file
     * @return void
     */
    public function setServicesFile(string $file)
    {

        $this->servicesFile = $file;
        
    }

    
    /**
     * Call all functions with static syntax
     * 
     * ??? Use currente instance of iod container (load at the first instance)
     *
     * @param [type] $name
     * @param [type] $arguments
     * @return void
     */
    public static function __callStatic($name, $arguments)
    {

        $result = call_user_func_array([static::$instance, $name], $arguments);
        return $result;

    }

}

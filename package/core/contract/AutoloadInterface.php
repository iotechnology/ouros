<?php

namespace App\Core\Contract;

/**
 * Contract for autoloader
 */
interface  AutoloadInterface
{
    /**
     * Define traitement of autoloader
     *
     * @param [type] $class
     * @return void
     */
    public function autoloader($class);

    /**
     * registry this autoloader on pl_autoload_register
     *
     * @return void
     */
    public function register();

}
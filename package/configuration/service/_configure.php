<?php
namespace App\Configuration\Service;

use App\Configuration\Contract\ConfigRegisterInterface;

class _configure implements ConfigRegisterInterface
{

    /**
     * Contain current instance of iod container
     *
     * @var App\Core\Hub\Iod
     */
    private $app;

    /**
     * Contain all common path of the application
     *
     * @var App\Hub\Core\Dictionary
     */
    private $dictionary;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $repositories;

    public function __construct()
    {
        $this->repositories = [];
        $this->dictionary = new Dictionary();
    }

    /**
     * Undocumented function
     *
     * @param string $repository
     * @param string $filename
     * @return void
     */
    public function load(string $repository ,string $filename)
    {
 
        if(is_file($repository.$filename)) {
            include $repository.$filename;
        } else {
            throw new \Exception('File '.$filename.' can\'t be found in '.$repository);
        }

    }

    /**
     * Undocumented function
     *
     * @param [type] $repository
     * @return void
     */
    public function addConfigRepository($repository) : void
    {
        if(is_dir($repository)) {
            $this->repositories[] = $repository;
        } else {
            throw new \Exception('Repository '.$repository.' can\'t be found');
        }
    }

    public function config($args = null)
    {
        
        $this->addConfigRepository(config);

        $regex = "#.*\.inc\.php#";
        foreach($this->repositories as $repo){
            $filenames = scandir($repo);
            foreach($filenames as $filename){
                if(preg_match($regex, $filename)) {
                    $this->load($repo, $filename);
                }
            }
        }
        
    }

    public function __call(string $name, $args)
    {
        $service = get($name);
        if(!$service instanceof ConfigRegisterInterface)
            throw new \RuntimeException(
                $name . " class instance must be implements ConfigRegisterInterface",
                500
            );
        
        $service->config();

    } 


}
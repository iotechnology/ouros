<?php

namespace App\Configuration\Contract;

interface ConfigRegisterInterface
{

    /**
     * Register configuration folder of the class
     *
     * @return void
     */
    public function config($args = null);



}
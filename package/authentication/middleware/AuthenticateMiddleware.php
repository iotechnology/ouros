<?php

namespace App\Authentication\Middleware;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Authentication\Hub\Authentication;

class AuthenticateMiddleware  extends Middleware
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {

        if(isset($request->getAttribute(uri)['options']['gate']))
            Authentication::gate($request->getAttribute(uri)['options']['gate']);

        if(Authentication::notRequire($request) || Authentication::guest($request) === false )
        {

        }
        else
        {
            if(!isset($request->getAttribute(uri)['options']['redirect']))
                throw new \Exception(
                    "Forbbiden",
                    404
                );
            
            header('Location: '.url($request->getAttribute(uri)['options']['redirect']));
            
        }
        //return $request->withAttribute('Auth' , 'Authnetication');

    }

}
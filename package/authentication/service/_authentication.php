<?php
namespace App\Authentication\Service;

use App\Authentication\Contract\AuthenticationInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Configuration\Contract\ConfigRegisterInterface;
use App\Authentication\Hub\Gate;
use \GuzzleHttp\Psr7\ServerRequest;

class _authentication implements ConfigRegisterInterface
{

    protected $request;

    protected $isConnect;

    protected $useGate;

    protected $gates = [];

    protected $providers = [
        'user' => [
            'model' => 'Model\\Authentication\\User',
        ],
    ];

    /**
     * Check if user can pass through the gate
     *
     * @param ServerRequestInterface $request
     * @return void
     */
    private function authorize(ServerRequest $request)
    {

        if(!isset($this->useGate))
            $this->gate();

        $driver = $this->useGate->getDriver();
        return $driver->authorize($request);

    }

    /**
     * Try to connect with the current gate
     *
     * @param ServerRequestInterface $request
     * @return void
     */
    public function connect(?array $credentials = null)
    {
        $driver = $this->useGate->getDriver();
        return $driver->connect($credentials);
    }

    public function logout()
    {

        if(!session_destroy())
            throw new \RuntimeException(
                "Error when try to destroy session",
                500
            );
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time()-3600, '/');
        }

        if(isset($_COOKIE['remember_token']))
            setcookie('remember_token', '', time() - 3600, '/');

    }

    /**
     * Check if user is connect like an guest
     * return true if not connect and cannot completed the current gate
     * return false
     *
     * @return boolean true/false
     */
    public function guest(ServerRequest $request)
    {

        $this->isConnect = $this->authorize($request);
        return !$this->isConnect;

    }

    public function notRequire(ServerRequest $request)
    {
        return $this->useGate->inExceptPath(\str_replace('url=','',$request->getUri()->getQuery()));
    }

    /**
     * Set the current gate to use for the next traitement
     * if the name is not define gate method return the first gate register 
     * @param string|null $name
     * @return void
     */
    public function gate(?string $name = null)
    {

        if(!isset($name))
            $name = array_key_first($this->gates);

        if(!isset($this->gates[$name]))
            throw new \Exception(
                $name . " gate not exist",
                500
            );
        $this->useGate = $this->gates[$name];
        return $this->gates[$name];

    }



    /**
     * Config method section
     */

    public function addGate(string $name, array $params)
    {
        $this->gates = array_merge($this->gates, [$name => new Gate($name, $params)]);
    }

    public function config($args = null)
    {

        $config = config.config_auth;
        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );
        
        
        $result = require_once $config;
        foreach($result as $k => $v)
        {
            if($k == 'gates')
            {

                foreach($v as $gk => $gv)
                {
                    $this->addGate($gk, $gv);
                }
            }
        }

    }

}
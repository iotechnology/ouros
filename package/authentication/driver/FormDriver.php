<?php

namespace App\Authentication\Driver;

use Psr\Http\Message\ServerRequestInterface;

class FormDriver extends Driver
{

    public function connect(array $credentials, ?string $rememberToken = null)
    {

        $user = $this->getUser($credentials, $rememberToken);

        if(is_null($user))
            return false;

        $this->initSession();
        $this->initToken($user);

        $_SESSION['login_'] = $user->getId();

        return true;
        
    }

    public function authorize(ServerRequestInterface $request)
    {

        $this->initSession();

        if(isset($_SESSION['login_']) && !empty($_SESSION['login_']))
        {

            $user = $this->model->getById($_SESSION['login_']);

        }
        else
        {

            $cookies = $request->getCookieParams();
            if(!isset($cookies['remember_token']) || empty($cookies['remember_token']))
                return false;
            
            $rememberToken = $cookies['remember_token'];
            return $this->connect([], $rememberToken);
            
        }
        if(is_null($user))
            return false;

        return true;
    }

}
<?php

namespace App\Authentication\Driver;

use App\Authentication\Contract\DriverInterface;
use Psr\Http\Message\ServerRequestInterface;

class Driver implements DriverInterface
{

    protected $model;

    protected $useToken;

    protected $realm;

    protected $scope;

    public function __construct(? array $params)
    {

        if(isset($params['model']) && !is_null($params['model']))
        {
            $object = $params['model'];

            if(!class_exists($object))
            {
                $object = 'Model\\Authentication\\'. ucfisrt($params['model']);
            
                if(!class_exists($object))
                    throw new \InvalidArgumentException(
                        $params['model'] . " cannot be resolved",
                        500
                    );
            }
            $this->model = new $object();
        }

        if(isset($params['token']) && !is_null($params['token']))
        {
            $this->useToken = $params['token'];
        }

        if(isset($params['realm']) && !is_null($params['realm']))
        {
            $this->realm = $params['realm'];
        }

        if(isset($params['scope']) && !is_null($params['scope']))
        {
            $this->scope = $params['scope'];
        }
        else
        {
            $this->scope = '/';
        }

    }

    public function connect(array $credentials)
    {

    }

    public function authorize(ServerRequestInterface $request)
    {

    }

    protected function getUser(array $credentials, ?string $rememberToken = null)
    {

        if(!empty($credentials))
            $user = $this->model->getByCredentials($credentials);
        else if(isset($rememberToken) && !is_null($rememberToken) && (!isset($user) || is_null($user)))
            $user = $this->model->getByToken($rememberToken);
    
        return $user ?? null;
    
    }

    protected function initSession()
    {
        
        sstart(['cookie_path' => $this->scope]);

        if(!shas('date'))
            sput('date', date('Y-m-d H:i:s'));

    }

    protected function initToken($user)
    {

        if(isset($this->useToken) && !empty($this->useToken) && $this->useToken == true)
        {

            $token = Token::generate('0');
            $this->model->registerNewToken($user, $token);
            setcookie('remember_token', $token, time()+ 60 * 10, $this->scope);

        }


    }

}
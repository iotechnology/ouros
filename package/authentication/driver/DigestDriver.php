<?php

namespace App\Authentication\Driver;

use App\Authentication\Contract\DriverInterface;
use Psr\Http\Message\ServerRequestInterface;

class DigestDriver implements DriverInterface
{

    public function connect(array $credentials, ?string $rememberToken = null)
    {

        if($this->useToken && isset($rememberToken))
        {
            $user = $this->getUser([], $rememberToken);

            if(!is_null($user))
                return true;
        }

        if(isset($_SESSION['auth']) && $_SESSION['auth'] && isset($_SERVER['PHP_AUTH_USER']))
        {

            $_SESSION['auth'] = false;
            $credentials = ['login' => $_SERVER['PHP_AUTH_USER'], 'password' => $_SERVER['PHP_AUTH_PW']];

            $user = $this->getUser($credentials);

            if(is_null($user))
                return false;
    
            $this->initSession();
            $this->initToken($user);
    
            $_SESSION['login_'] = $user->getIdUser();
                
            return true;

        }

        if(!isset($this->realm))
            throw new \Exception(
                "Realm must be define for basic driver",
                500
            );

        if ((!$this->useToken || (!isset($rememberToken) || is_null($rememberToken))) && !isset($_COOKIE['login_'])) 
        {

            $_SESSION['auth'] = true;
            header('WWW-Authenticate: Basic realm="'.$this->realm.'"');
            header('HTTP/1.0 401 Unauthorized');
            return false;

        }



    }

    public function authorize(ServerRequestInterface $request)
    {

        $this->initSession();

        if(isset($_SESSION['login_']) && !empty($_SESSION['login_']))
        {

            $user = $this->model->getById($_SESSION['login_']);

        }
        else
        {

            $cookies = $request->getCookieParams();
            if(!isset($cookies['remember_token']) || empty($cookies['remember_token']))
                return $this->connect([]);
                
            $rememberToken = $cookies['remember_token'];
            return $this->connect([], $rememberToken);
            
        }
        if(is_null($user))
            return false;

        return true;

    }

}
<?php

namespace App\Authentication\Contract;

use Psr\Http\Message\ServerRequestInterface;

interface AuthenticableInterface
{

    public function registerNewToken($user, $token);

    public function getById($id);

    public function getByToken($token);

    public function getByCredentials(array $credentials);

}
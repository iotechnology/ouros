<?php

namespace App\Authentication\Contract;

use Psr\Http\Message\ServerRequestInterface;

interface DriverInterface
{

    public function __construct(? array $params);

    public function connect(array $credentials);

    public function authorize(ServerRequestInterface $request);


}
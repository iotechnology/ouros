<?php

namespace App\Authentication\Contract;

use Psr\Http\Message\ServerRequestInterface;

interface AuthenticationInterface
{

    /**
     * @param ServerRequestInterface $request
     * 
     * @return void
     */
    public function authorize(ServerRequestInterface $request);

}
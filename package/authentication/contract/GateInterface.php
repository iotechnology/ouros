<?php

namespace App\Authentication\Contract;

interface GateInterface
{


    public function __construct(string $name, array $config);

    /**
     * Return the name of this gate
     *
     * @return string
     */
    public function getName() : string;

    /**
     * Return an instance of the driver
     *
     * @return DriverInterface
     */
    public function getDriver();

    /**
     * Return array of this gate configuration
     *
     * @return array
     */
    public function getConfig();

    /**
     * Set new Name for this gate
     *
     * @param string $name
     * @return void
     */
    public function setName(string $name);

    /**
     * Set new driver for this gate
     *
     * @param string $driver
     * @return void
     */
    public function setDriver(string $driver);

    /**
     * Set new config for this gate
     *
     * @param array $config
     * @return void
     */
    public function setConfig(array $config);


}
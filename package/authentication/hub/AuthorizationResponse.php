<?php

namespace App\Authentication\Hub;

use App\Authentication\Contract\AuthorizationResponseInterface;

class AuthorizationResponse implements AuthorizationResponseInterface
{

    public function __construct()
    {

    }

    public function access()
    {

    }

}
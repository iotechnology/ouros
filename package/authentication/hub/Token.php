<?php

namespace App\Authentication\Hub;

class Token
{

    public static function generate(string $key)
    {
        return hash_hmac('sha256', \bin2hex(\random_bytes(16)) ,$key);
    }


}
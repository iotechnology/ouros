<?php

namespace App\Authentication\Hub;

use App\Authentication\Contract\DriverInterface;

class Gate
{

    private $name;

    private $driver;//Form / Basic / Digest

    private $config;

    private $except;

    public function __construct(string $name, array $config)
    {

        $this->name = $name;

        $this->driver = 'token';
        if(isset($config['driver']))
            $this->driver = $config['driver'];
        unset($config['driver']);

        if(isset($config['except']) && \is_array($config['except']))
            $this->except = $config['except'];

        $this->config = $config;
    }

    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Return an instance of the driver
     *
     * @return void
     */
    public function getDriver()
    {

        /**
         * Check existance of class
         */
        if(!class_exists($this->driver))
        {
            $name = 'App\\Authentication\\Driver\\' . ucfirst($this->driver) . 'Driver';
            if(!class_exists($name))
                throw new \Exception(
                    $name . " driver not exist",
                    500
                );
        }

        /**
         * Check if the interface implemented was DriverInterface
         * If is not instance of Driver interface throw new Exception with internal error code
         * Else continue with instanciation of the class
         */
        $class = new \ReflectionClass($name);
        if(!$class->implementsInterface('App\Authentication\Contract\DriverInterface'))
            throw new \Exception(
                $name . " must be implements App\Authentication\Contract\DriverInterface",
                500
            );

        return new $name($this->config);

    }

    public function getConfig()
    {
        return $this->config;
    }

    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    public function setDriver(string $driver)
    {
        $this->driver = $driver;
        return $this;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function inExceptPath(string $path)
    {
        return in_array($path, $this->except);
    }

}
<?php

namespace App\Authentication\Hub;

class Password 
{

    private $password;

    private $algo;

    private $salt;

    public function __construct($password, $algo = PASSWORD_DEFAULT, $salt = default_salt_password)
    {
        $this->password = $password;
        $this->algo = $algo;
        $this->salt = $salt;
    }

    public function hash()
    {
        return password_hash($this->password,$this->algo, ['salt' => $this->salt]);
    }
    
}
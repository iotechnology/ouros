<?php

namespace App\Factory\Hub;

use App\Factory\Contract\FactoryInterface;

abstract class Factory implements FactoryInterface
{

    /**
     * Construct and fill factory with iod container
     *
     * @param App\Core\Service\Iod $iod
     */
    public function __construct()
    {

        

    }


    public function create($class , $args = null)
    {

    }

    public function register()
    {

    }

}
<?php

namespace App\Factory\Contract;

interface FactoryInterface
{

    public function create($class, $args = null);

    public function register();

}
<?php

namespace App\Management\Hub;

use ReflectionClass;

use App\Core\Contract\MiddlewareInterface;
use App\Core\Hub\Blind;
use \GuzzleHttp\Psr7\Response;
use \GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Configuration\Contract\ConfigRegisterInterface;

/**
 * Dispatch middlewares
 */
class MiddlewareManager implements RequestHandlerInterface, ConfigRegisterInterface
{

    /**
     * Stack of middlewares
     *
     * @var array
     */
    private $middlewares = [];

    /**
     * Stack of detach middleware
     */
    private $detach = [];
    /**
     * index of middleware need to call
     *
     * @var int
     */
    private $index;

    /**
     * Response
     *
     * @var Psr\Http\Message\ResponseInterface
     */
    private $response;
    

    /**
     * Constructor initialize Response and first id middleware
     */
    public function __construct()
    {            

        $this->index = 0;
        $this->response = new Response();
        
    }

    /**
     * Add one middleware
     *
     * @param  callable/MiddlewareInterface $middleware
     * @return void
     */
    public function put($middleware)
    {
        array_push($this->middlewares, $middleware);
    }

    /**
     * Add middlewares in stack $middlewares
     *
     * @param [type] $middlewares
     * @return void
     */
    public function puts(string $array ,$middlewares)
    {
        $this->$array = array_merge($this->$array, $middlewares);
    }

    /**
     * Handle current middleware
     *
     * @param ServerRequestInterface $request
     * @return void
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {

        $middleware = $this->getMiddleware();
        $this->index++;

        if(isset($middleware)) {
            
            if(is_callable($middleware)) {
                return $middleware($request, $this->response, [$this, 'process']);
            }elseif($middleware instanceof Blind && $middleware->getService() instanceof MiddlewareInterface) {
                return $middleware::process($request, $this);
            }elseif($middleware instanceof Middleware) {
                return $middleware->process($request, $this);
            }
            
        }else{

            return $this->response;

        }

    }

    public function handleDetach(string $name, $request)
    {

        \setRequest($request);
        if(!isset($this->detach[$name]))
            throw new \InvalidArgumentException(
                $name . " is not a valid name of detach middleware",
                500
            );

        $realname = $this->detach[$name];

        if(!class_exists($realname))
            throw new \RuntimeException(
                $realname. " not exist.", 500
            );
            
        $middleware = new $realname();

        if(is_callable($middleware)) {
            return $middleware($request, $this->response, [$this, 'process']);
        }elseif($middleware instanceof Blind && $middleware->getService() instanceof MiddlewareInterface) {
            return $middleware::process($request, $this);
        }elseif($middleware instanceof MiddlewareInterface) {
            return $middleware->process($request, $this);
        }

    }


    /**
     * Return current middleware
     *
     * @return void/CallBack/MiddlewareInterface
     */
    public function getMiddleware()
    {

        if(isset($this->middlewares[$this->index])) {
            if(\is_string($this->middlewares[$this->index])) $this->middlewares[$this->index] = $this->resolve($this->index);
            return $this->middlewares[$this->index];
        }
        return null;

    }

    protected function resolve(int $index)
    {
        
        if(!class_exists($this->middlewares[$index]))
            throw new \RuntimeException(
                $this->middlewares[$index]. " not exist.", 500
            );
        return new $this->middlewares[$index]();
    }

    /**
     * Set index of middleware at the next of object passed in param
     *
     * @return void
     */
    public function setNextIndexMiddleware($object)
    {
        foreach($this->middlewares as $key => $middleware)
        {
            if($middleware === $object)
            {
                $this->index = $key+1;
                break;
            }
        }
    }

    public function config($args = null)
    {


    }

}

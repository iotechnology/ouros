<?php

namespace App\Management\Hub;

use App\Management\Handler\CallbackHandler;
use App\Management\Contract\HandlerInterface;
use App\Management\Contract\HandlerManagerInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Undocumented class
 */
abstract class HandlerManager implements HandlerManagerInterface
{

    /**
     * Handler instance storage
     *
     * @var array
     */
    protected $handlerInstances = [];

    /**
     * Handler name storage
     *
     * @var array
     */
    protected $handlerClasses = [];

    /**
     * handle manager
     *
     * @param ServerRequestInterface $request
     * @return void
     */
    public function handle(ServerRequestInterface $request){
        
    }

    /**
     * Add handler with handler class name
     *
     * @param  string/callable $class
     * @return void
     */
    public function pushHandler($class)
    {
        $this->handlerClasses[] = $class;
    }

    /**
     * Add multiple handler by array of class name (string)
     *
     * @param  array $class
     * @return void
     */
    public function pushHandlers(array $class)
    {
        $this->handlerClasses = array_merge($this->handlerClasses, $class);
    }

    /**
     * Add Handler at the end of Handlers Array
     *
     * @param  HandlerInterface $handler
     * @return void
     */
    public function pushHandlerObject($handler)
    {

        $this->handlerInstances[] = $this->resolveHandler($handler);

    }

    /**
     * Remove and return first handler registered
     *
     * @return void
     */
    public function shiftHandler()
    {
        return array_shift($this->handlerClasses);
    }

    /**
     * Remove and return last handler registered
     *
     * @return void
     */
    public function popHandler()
    {
        return array_pop($this->handlerClasses);
    }

    /**
     * Check class of handler and is it callable to register his callback
     * !!! Throw new exception if is not available Handler
     *
     * @param  HandlerInterface $handler
     * @return void
     */
    private function resolveHandler($handler)
    {

        if (is_callable($handler)) {
            $handler = new CallbackHandler($handler);
        }
        if (!$handler instanceof HandlerInterface) {
            throw new \InvalidArgumentException(
                "Handler must be a callable, or instance of "
                . "App\\Management\\Handler\\HandlerInterface"
            );
        }
        return $handler;

    }

    /**
     * Remove first Handler registered
     *
     * @return void
     */
    public function rmFHandlerObject()
    {
        array_shift($this->handlerClasses);
    }

    /**
     * Remove Last Handler registered
     *
     * @return void
     */
    public function rmLHandlerObject()
    {
        array_pop($this->handlerClasses);
    }

    /**
     * Initialize all Handlers set with string
     *
     * @return void
     */
    protected function initializeHandlers()
    {
        foreach($this->handlerClasses as $class){

            if((is_string($class) && !class_exists($class)) 
            || (!is_string($class) && !is_callable($class)))
                throw new \InvalidArgumentException(
                    $class.' must be valid class name or valid callable'
                );

            if(is_string($class))
                $this->pushHandlerObject(new $class());
            
            if(is_callable($class))
                $this->pushHandlerObject($class);
            
        }
    }

}

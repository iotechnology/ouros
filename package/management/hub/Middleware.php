<?php

namespace App\Management\Hub;

use App\Core\Contract\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

abstract class Middleware implements MiddlewareInterface
{

    /**
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * 
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {



    }


}
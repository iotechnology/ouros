<?php

namespace App\Management\Contract;

/**
 * Undocumented interface
 */
interface HandlerInterface
{

    /**
     * Undocumented function
     *
     * @return void
     */
    public function handle();

}

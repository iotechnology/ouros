<?php

namespace App\Management\Contract;

use Psr\Http\Message\ServerRequestInterface;

interface HandlerManagerInterface
{
    
    public function handle(ServerRequestInterface $request);

}
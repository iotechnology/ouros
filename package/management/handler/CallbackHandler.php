<?php

namespace App\Management\Handler;

use App\Management\Contract\HandlerInterface;

/**
 * Undocumented class
 */
class CallbackHandler implements HandlerInterface
{
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $callable;

    /**
     * Undocumented function
     *
     * @param [type] $handler
     */
    public function __construct($handler)
    {

        if(!is_callable($handler)) {
            throw new \InvalidArgumentException(
                'Argument to '.__METHOD__.' must be valid callable'
            );
        }
        $this->callable = $callable;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function handle()
    {
        $callable = $this->callable;
        $callable();
    }


}

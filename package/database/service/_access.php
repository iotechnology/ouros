<?php

namespace App\Database\Service;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

use App\Database\Contract\AccessInterface;

use App\Configuration\Contract\ConfigRegisterInterface;

class _access implements AccessInterface, ConfigRegisterInterface
{

    private $entityManager = [];
    private $config = [];
    private $options = [
        'default' => [
            'path' => [],
            'devMode' => true,
            'proxyDir' => null,
            'cache' => null,
            'useSimpleAnnotationReader' => false
        ]
    ];

    private $conn = [
            'default' => [
                'driver' => 'pdo_mysql',
                'user' => 'root',
                'password' => '',
                'dbname' => 'test',
            ],
        ];

    public function __construct(){

        $this->options['default']['path'] = array(dirname(dirname(dirname(__DIR__)))."/src/model/entity/");
        
    }

    public function set(string $name ,$settings = null){

        if(isset($settings) && !\is_array($settings)){
            throw new \InvalidArgumentException(
                "Argument to ".__METHOD__." must be array"
            );
        }

        if(isset($settings) && \is_array($settings)){
            if(!isset($settings['dbparams']) && !isset($settings['options'])){
                throw new \InvalidArgumentException(
                    "Argument to ".__METHOD__." must be 2 dimensional array ['dbparams'[],'options'[]]"
                );
            }

            if(isset($settings['dbparams'])){
                foreach($settings['dbparams'] as $op => $v){
                   if(isset($this->conn[$name][$op])) $this->conn[$name][$op] = $v;
                   else $this->conn[$name][] = [$op => $v];
                }
            }
            if(isset($settings['options'])){
                foreach($settings['options'] as $op => $v){
                    if(isset($this->options[$name][$op])) $this->options[$name][$op] = $v;
                    else $this->options[$name][] = [$op => $v];
                 }
            }
        }
    }

    public function connect(string $name) : bool{

        if(!isset($this->entityManager[$name])){
            $options = $this->options[$name];
            try{
                $this->config[$name] = Setup::createAnnotationMetadataConfiguration($options['path'], $options['devMode'], $options['proxyDir'], $options['cache'], $options['useSimpleAnnotationReader']);
                $this->entityManager[$name] = EntityManager::create($this->conn[$name], $this->config[$name]);
                return true;
            }catch(\Exception $e){
                return false;
            }
        }
        return true;
    }

    public function get($name = null) {

        if(is_null($name)){
            $name = 'default';
        }
        else if(!\is_string($name))
        {  
            var_dump($name);
            throw new \Exception(
                'You try to access database with invalid arguments : '.$name, 500
            );
        }
        if(!$this->connect($name))

            throw new \Exception(
                'We cannot connect to database '.$name
            );
        return $this->entityManager[$name];
     
    }

    public function config($args = null){
        $config = config.config_database;
        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );
        
        
        $result = require_once $config;
        foreach($result as $name => $params)
        {
            $this->set($name, $params);
        }

    }

}
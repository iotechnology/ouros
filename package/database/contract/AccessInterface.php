<?php

namespace App\Database\Contract;

interface AccessInterface
{

    public function set(string $name ,$settings = null);

    public function connect(string $name);

    public function get($name = null);

}
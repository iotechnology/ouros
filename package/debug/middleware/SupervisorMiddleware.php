<?php

namespace App\Debug\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Undocumented class
 */
class SupervisorMiddleware extends Middleware
{

    /**
     * function initialize Debboger Manager before launch Next Handler
     *
     * @param  ServerRequestInterface  $request
     * @param  RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {

        Debug::register();
        try
        {

            $response = $handler->handle($request);
                
        }
        catch(\Exception $exception)
        {


            if(Debug::isDev())
            {

                throw $exception;

            }
            else
            {

                $handler->setNextIndexMiddleware($this);

                $errorRequest = $request->withAttribute(url , 'error/public')->withMethod('GET');
                $response = $handler->handle($errorRequest->withAttribute(exception_code , $exception->getCode()));

                if($exception->getCode() < 100)
                    $code = 500;
                else
                    $code = $exception->getCode();
                $response = $response->withStatus($code);

            }

        }

        return $response;
        
    }

}

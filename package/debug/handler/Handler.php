<?php

namespace App\Debug\Handler;

use App\Management\Contract\HandlerInterface;

/**
 * Undocumented class
 */
abstract class Handler implements HandlerInterface
{

    /**
     * 
     */
    const EXIT_ = 0xFF;

    /**
     * 
     */
    const RESOLVE_ = 0x45;

    /**
     * 
     */
    const DONE_ = 0xAA;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $exception;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $analyser;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $manager;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function handle()
    {

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function setRun()
    {

    }

    /**
     * Undocumented function
     *
     * @param [type] $exception
     * @return void
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Undocumented function
     *
     * @param [type] $analyser
     * @return void
     */
    public function setAnalyser($analyser)
    {
        $this->analyser = $analyser;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getAnalyser()
    {
        return $this->analyser;
    }

    /**
     * Undocumented function
     *
     * @param [type] $manager
     * @return void
     */
    public function setManager($manager)
    {

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getManager()
    {
        return $this->manager;
    }


}

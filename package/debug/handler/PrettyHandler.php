<?php

namespace App\Debug\Handler;

use App\Debug\Handler\Handler;
use App\Rendering\Blind\ViewManager;
use App\Debug\Hub\Analyser;
use App\Debug\Hub\Frame;

/**
 * Undocumented class
 */
class PrettyHandler extends Handler
{

    /**
     * Undocumented variable
     *
     * @var array
     */
    private $editors = [

        "sublime"  => "subl://open?url=file://%file&line=%line",
        "textmate" => "txmt://open?url=file://%file&line=%line",
        "emacs"    => "emacs://open?url=file://%file&line=%line",
        "macvim"   => "mvim://open/?url=file://%file&line=%line",
        "phpstorm" => "phpstorm://open?file=%file&line=%line",
        "idea"     => "idea://open?file=%file&line=%line",
        "vscode"   => "vscode://file/%file:%line",
        "atom"     => "atom://core/open/file?filename=%file&line=%line",
        "espresso" => "x-espresso://open?filepath=%file&lines=%line",
        
    ];

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $editor = null;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function handle()
    {

            $exception = $this->getException();
            $frames = $this->getAnalyser()->getFrameCollection()->getArray();
            $editor = $this->getEditor('vscode');
            $that = $this;

            ViewManager::setLayout('Error');
            ViewManager::set(compact(['exception','frames','editor', 'that']));
            ViewManager::extractInLayout();

            ViewManager::setView(['prefix' => 'error' ,'controller' => 'debug', 'action' => 'index']);

            echo ViewManager::render();

            return Handler::EXIT_;
    }

    /**
     * Undocumented function
     *
     * @param string $editor
     * @return void
     */
    public function setEditor(string $editor)
    {

        if(!isset($editor) || $editor === '') {
            throw new InvalidArgumentException(
                'Argument to ' . __METHOD__ . ' must be string and not empty'
            );
        }
        if(isset($this->editors[$editor])) {
            return ($this->editor = $this->editors[$editor]) != null;
        }

        throw new UnexpectedValueException(
            $editor. ' not found in ' . __METHOD__
        );

    }

    /**
     * Undocumented function
     *
     * @param string $editor
     * @param [type] $frame
     * @return void
     */
    public function getEditor(string $editor, $frame = null)
    {
        if(!isset($editor)) {
            throw new InvalidArgumentException(
                'Argument [editor] to ' . __METHOD__ . ' cannot be null'
            );
        }
        $this->setEditor($editor);
        if(isset($frame) && $frame != null) {
            if(!$frame instanceof Frame || !is_array($frame) && !isset($frame['file']) && !isset($frame['line'])) {
                throw new InvalidArgumentException(
                    'Argument [$frame] to ' . __METHOD__ . ' must be a frame or array containing [\'file\',\'line\']'
                );
            }
            if($frame instanceof Frame) {
                $frame = [$frame->getFile(), $frame->getLine()];
            }
        }else{
            $frame = [$this->getException()->getFile(), $this->getException()->getLine()];
        }
        if(is_callable($this->editor)) {
            $result = call_user_func($this->editor, $frame);
            if(is_string($result)) {
                return [
                    'ajax' => false,
                    'url' => $result,
                ];
            }
            if(!is_array($result)) {
                throw new UnexpectedValueException(
                    'Callback in ' . __METHOD__ . 'must return an array containing [\'ajax\'(bool),\'url\'(string)] or just string containing url'
                );
            }
            return [
                'ajax' => isset($result['ajax']) ? $result['ajax'] : false,
                'url' => isset($result['url']) ? $result['url'] : null,
            ];
        }
        return [
            'ajax' => false,
            'url' => preg_replace(['/%file/','/%line/'], $frame, $this->editor),
        ];
        
    }

    /**
     * Undocumented function
     *
     * @param [type] $editor
     * @param [type] $item
     * @return void
     */
    public function pushEnableEditor($editor, $item)
    {
        
        if((!is_string($item) || !preg_match('/(%file)(%line)/', $item)) || !is_callable($item)) {
                throw new InvalidArgumentException(
                    'Argument [url] to ' . __METHOD__ . ' must be string containing \'%file\' and \'%line\' or must be callable'
                );
        }else{

        }
        if(!is_string($editor)) {
            throw new InvalidArgumentException(
                'Argument [editor] to ' . __METHOD__ . ' must be string'
            );
        }

        $this->editors[] = [$editor => $url];

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getHrefEditor()
    {

    }

    /**
     * Undocumented function
     *
     * @param array $vars
     * @return void
     */
    public function printEnv(array $vars)
    {
        
        foreach($vars as $k => $v){
            if(is_array($v)) {
                if($k != "GLOBALS") {
                    echo "<div class=\"Env-section\">";
                    $k = trim($k, "_");
                    echo "<h5 class=\"Env-section-title\">$k</h5>";
                    if(!empty($v)) 
                    {
                        $this->printEnv($v);
                    } 
                    else
                    {
                        echo "<div class=\"Env-section-sub\">";
                        echo "<p class=\"Env-section-sub-text\"> Empty </p>";
                        echo "</div>";
                    }
                    echo "</div>";
                }
            }
            else if(!\is_object($v)){
                if(!is_string($v)) 
                { 
                    $v = strval($v);
                }
                echo "<div class=\"Env-section-sub\">";
                echo "<h5 class=\"title\">$k</h5>";

                if($k == "SERVER_SIGNATURE") 
                { 
                    echo $v;
                } 
                else 
                {
                    echo "<p  class=\"text\">$v</p>";
                }
                echo "</div>";
            }  
        }
        
    }
}

<?php

namespace App\Debug\Service;

use App\Debug\Hub\Analyser;
use App\Debug\Handler\Handler;
use App\Configuration\Contract\ConfigRegisterInterface;

/**
 * Use to rewrite system handlers
 */
const EXCEPTION_HANDLER = "handleException";
const ERROR_HANDLER = "handleError";
const SHUTDOWN_HANDLER = "handleShutdown";

/**
 * Use to add default user handlers
 */
const VIEW_HANDLER = 'App\Debug\Handler\PrettyHandler';
const LOG_HANDLER = 'App\Debug\Handler\LogHandler';
const JSON_HANDLER = 'App\Debug\Handler\JsonHandler';
const XML_HANDLER = 'App\Debug\Handler\XmlHandler';

class _debugmanager extends Manager implements ConfigRegisterInterface
{

    /**
     * Undocumented variable
     *
     * @var array
     */
    private $options = [
        'dev' => false,
        'exceptionHandler' => [],
        'errorHandler' => [],
        'shutdownHandler' => [],
        'viewHandler' => VIEW_HANDLER,
        'logHandler' => LOG_HANDLER,
        'jsonHandler' => JSON_HANDLER,

    ];

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    private $isRegister = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    private $allowQuit = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    private $canThrow = true;

    /**
     * Undocumented variable
     *
     * @var array
     */
    private $classHandlers = [];

    /**
     * Undocumented variable
     *
     * @var array
     */
    private $handlers = [];

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $analyser;

    public function __construct()
    {

        $this->options = array_merge(
            $this->options, [
                'dev'               => dev_mode,
                'exceptionHandler'  => [$this,EXCEPTION_HANDLER],
                'errorHandler'      => [$this,ERROR_HANDLER],
                'shutdownHandler'   => [$this,SHUTDOWN_HANDLER],
            ]
        );

    }

    /**
     * Return option array
     *
     * @return void
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Use to define all params from outer class  
     * Recommande to use before all process
     * 
     * !!! Throw an exception when enter unknow key or have not a good type or class
     * 
     * @param  array $options
     * @return void
     */
    public function setParams(array $options)
    {

        foreach($options as $key => $value){

            if(!isset($this->options[$key])) {
                throw new \InvalidArgumentException(
                    'Argument '.$key.' to ' . __METHOD__ . ' not exist in parameters'
                );
            }

            if(!(gettype($value) === gettype($this->options[$key]))) {
                throw new \InvalidArgumentException(
                    'Argument '.$key.' to ' . __METHOD__ . ' have not avaible type. '.$key.' is type of '.gettype($value).' and type '.gettype($this->options[$key]).' is expected'
                );
            }

            if(is_object($value) && !(get_class($value) === get_class($this->options[$key]))) {
                throw new \InvalidArgumentException(
                    'Argument '.$key.' to ' . __METHOD__ . ' have not avaible class. '.$key.' is class of '.get_class($value).' and class '.get_class($this->options[$key]).' is expected'
                );
            }

            $this->options[$key] = $value;

        }
        
    }

    /**
     * Call on not catch exception and dispatch work on regiter handler
     *
     * @param  Exception $exception get by system on non catch exception
     * @return void
     */
    public function handleException($exception)
    {   

        $this->initializeHandlers();
        
        $analyser = new Analyser($exception);

        \ob_start();
        
        try{
            
            foreach(array_reverse($this->handlerInstances) as $handler){
 
                $handler->setManager($this);
                $handler->setAnalyser($analyser);
                $handler->setException($exception);

                $response = $handler->handle($exception);
                $ContentType = method_exists($handler, 'contentType') ? $handler->contentType() : null;
                if($response == Handler::RESOLVE_ || $response == Handler::EXIT_) {
                    break;
                }
                else if($response != Handler::DONE_) {
                    throw new \UnexpectedValueException(
                        'Response of handler must be Handler::RESOLVE_ | EXIT_ | DONE_'
                    );
                }
            }
            $exitQuery = ($response == (Handler::EXIT_ && $this->allowQuit()));

        }finally{

            $output = ob_get_contents();
            ob_get_clean();
        }

        if($this->options['dev']) {

            if($exitQuery) {
                
                while(ob_get_level() > 0){
                    ob_end_clean();
                }

                if(headers_sent() && $ContentType) {
                    header("Content-Type : {$ContentType}");
                }

            }
            
            echo $output;

        }

        if($exitQuery) {

            flush();
            exit(1);

        }

        return $output;
        
    }

    /**
     * Call on Error, if error level is fatal converte his on Exception
     *
     * @param  int    $level
     * @param  string $message
     * @param  string $file
     * @param  int    $line
     * @return void
     */
    public function handleError($level, $message, $file = null, $line = null)
    {
        
        if($level & error_reporting()) {

            $exception = new \ErrorException($message, $level, $level, $file, $line);

            if($this->canThrow) {
                throw $exception;
            }else{
                $this->handleException($exception);
            }

            return true;

        }

        return false;

    }

    /**
     * Call on shutdown to create new error with last error and call handleError to convert his on Exception
     *
     * @return void
     */
    public function handleShutdown()
    {

        $this->canThrow = false;
        
        $error = error_get_last();
        if($error && $this->isFatal($error['type'])) {

            $this->handleError(
                $error['type'],
                $error['message'],
                $error['file'],
                $error['line']
            );

        }

    }

    public function isDev(){
        return $this->options['dev'];
    }

    /**
     * Return if is possible to exit application or change and return new value
     *
     * @param  bool $exit
     * @return void
     */
    public function allowQuit($exit = null)
    {

        if(func_num_args() == 0) {

            return $this->allowQuit;

        }
        
        return $this->allowQuit = $exit;
    }

    /**
     * Init all needed handler system to dispatch registered user handler
     *
     * @return void
     */
    public function register()
    {

        if(!$this->isRegister) {

            set_exception_handler([$this,EXCEPTION_HANDLER]);
            set_error_handler([$this,ERROR_HANDLER]);
            register_shutdown_function([$this,SHUTDOWN_HANDLER]);

            $this->isRegister = true;
        }

        return $this;

    }

    /**
     * Disable manager handler to disable access of error/exception gesture
     *
     * @return void
     */
    public function unregister()
    {

        if ($this->isRegistered) {

            restore_exception_handler();
            restore_error_handler();

            $this->isRegistered = false;

        }

        return $this;

    }

    /**
     * Check Fatality of error level
     * 
     * @param  [type] $level
     * @return boolean
     */
    public function isFatal($level)
    {

        $errors = E_ERROR;
        $errors |= E_PARSE;
        $errors |= E_CORE_ERROR;
        $errors |= E_USER_ERROR;
        $errors |= E_COMPILE_ERROR;
        $errors |= E_RECOVERABLE_ERROR;
        return ($level & $errors) > 0;

    }

    public function config($args = null)
    {
        $config = config.config_debug;
        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );

        $result = require_once $config;
        foreach($result as $key => $value)
        {
            if($key == 'options') $this->setParams($value);
            else if($key == 'handlers') $this->pushHandlers($value);
        }
    }

}

<?php

namespace App\Debug\Hub;

/**
 * Undocumented class
 */
class FrameCollection
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $frames;

    /**
     * Undocumented function
     *
     * @param [type] $traces
     */
    public function __construct($traces)
    {
        $this->frames = [];
        $this->frames = array_map(
            function ($trace) {
                return new Frame($trace);
            }, $traces
        );
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function pop()
    {
        return array_pop($this->frames);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function shift()
    {
        return array_shift($this->frames);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return void
     */
    public function get($id = null)
    {
        if(!isset($id) && is_int($id) || is_array($id)) { 
            throw new \InvalidArgumentException(
                'Argument to ' . __METHOD__ . ' must be an integer or an array of integer and not empty'
            );
        }
        
        if(is_int($id)) { return $this->frames[$id];
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function count()
    {
        if(!empty($this->frames)) {
            return count($this->frames);
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getArray()
    {
        return $this->frames;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function reverse()
    {
        return $this->frames = array_reverse($this->frames);
    }

    /**
     * Undocumented function
     *
     * @param [type] $begin
     * @param [type] $end
     * @return void
     */
    public function slice($begin, $end)
    {
        return array_slice($this->frames, $begin, $end);
    }

    /**
     * Undocumented function
     *
     * @param [type] $callable
     * @return void
     */
    public function map($callable)
    {
        if(!isset($callable) || !is_callable($callable)) {
            throw new InvalidArgumentException(
                'Argument to ' . __METHOD__ . ' must be callable and not empty'
            );
        }
        return array_map($callable, $this->frames);
    }

    /**
     * Undocumented function
     *
     * @param array $frames
     * @return void
     */
    public function mergeFrames(array $frames)
    {
        $this->frames = array_merge($frames, $this->frames);
    }
    
    /**
     * Undocumented function
     *
     * @param FrameCollection $parentFrames
     * @return void
     */
    public function innerDiff(FrameCollection $parentFrames)
    {
        $diff = $this->frames;

        $parentFrames = $parentFrames->getArray();
        $p = count($parentFrames)-1;
        $i = count($diff)-1;
        for ($i ; $i >= 0 && $p >= 0; $i--, $p--) {
            if ($diff[$i] == $parentFrames[$p]) {
                unset($diff[$i]);
            }
        }
        return $diff;
    }

}

<?php

namespace App\Debug\Hub;

use App\Debug\Hub\FrameCollection;

/**
 * Undocumented class
 */
class Analyser
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $exception;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $previousAnalyser;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $previousException;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $frames;

    /**
     * Undocumented function
     *
     * @param [type] $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getFrameCollection()
    {
        if($this->frames === null) {
            $frames = $this->getTrace($this->exception);
        }

        foreach($frames as $k => $v){
            if(!isset($v['file']) || empty($v['file'])) {
                $file = '[internal]';
                $line = 0;
                $next_frame = (isset($v['file']) && !empty($frames[$k + 1])) ? $frames[$k + 1] : [];
                if($this->isValidFrame($next_frame)) {
                    $file = $next_frame['file'];
                    $line = $next_frame['line'];
                }
                $frames[$k]['file'] = $file;
                $frames[$k]['line'] = $line;
            }
            if(!isset($v['class']) || empty($v['class'])) {
                $frames[$k]['class'] = '[Internal]';
            }
            if(!isset($v['function']) || empty($v['function'])) {
                $frames[$k]['function'] = '[none]';
            }
        }

        $i = 0;
        foreach ($frames as $k => $v) {
            if ($v['file'] == $this->exception->getFile() && $v['line'] == $this->exception->getLine()) {
                $i = $k;
                array_splice($frames, 0, $i);
            }
        }
        $firstTrace = $this->converToArray($this->exception);
        array_unshift($frames, $firstTrace);

        $this->frames = new FrameCollection($frames);
        if($previousAnalyser = $this->getPreviousAnalyser()) {
            $childFrames = $this->frames;
            $parentFrames = clone $previousAnalyser->getFrameCollection();
            $parentFrames->mergeFrames($childFrames->innerDiff($parentFrames));
            $this->frames = $parentFrames;
        }

        return $this->frames;
        
        //$this->frames = array_merge($frames, $this->frames);



    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPreviousAnalyser()
    {
        if($this->previousAnalyser === null && ($previousException = $this->exception->getPrevious())) {
            $this->previousAnalyser = new Analyser($previousException);
        }
        return $this->previousAnalyser;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPreviousException()
    {
        if($this->previousException === null) {
            $this->previousException = [];
            $pe = $this->exception->getPrevious();
            while($pe !== null){
                $this->previousException[] = $pe;
                $pe = $pe->getPrevious();
            }
        }
        return $this->previousException;
    }

    /**
     * Undocumented function
     *
     * @return boolean
     */
    public function hasPreviousException()
    {
        return $this->previousAnalyser || $this->exception->getPrevious();
    }

    /**
     * Undocumented function
     *
     * @param [type] $exception
     * @return void
     */
    public function getTrace($exception)
    {
        $trace = $exception->getTrace();

        if(!$exception instanceof \ErrorExeption || !$this->isLevelFatal($exception->getCode()) || !extension_loaded('xdebug') || !xdebug_is_enabled()) {
            return $trace;
        }

        $stack = array_reverse(xdebug_get_function_stack());
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $trace = array_diff_key($stack, $trace);

        return $trace;
    }

    /**
     * Undocumented function
     *
     * @param  Exception $exception
     * @return void
     */
    protected function converToArray($exception)
    {
        if($exception instanceof \ErrorException) {
            return [
                'file'  => $exception->getFile(),
                'line'  => $exception->getLine(),
                'class' => null,
                'args'  => [],
            ];
        }
        
        return [
            'file'  => $exception->getFile(),
            'line'  => $exception->getLine(),
            'class' => get_class($exception),
            'args'  => [
                $exception->getMessage(),
            ],
        ];

    }

    /**
     * Check format of array to use like frame or to create array
     *
     * @param  array $frame
     * @return boolean
     */
    protected function isValidFrame(array $frame)
    {
        if (empty($frame['file']) || empty($frame['line'])) {
            return false;
        }
        if (empty($frame['function']) || !stristr($frame['function'], 'call_user_func')) {
            return false;
        }

        return true;
    }

}

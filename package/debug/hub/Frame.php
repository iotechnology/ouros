<?php

namespace App\Debug\Hub;

/**
 * Undocumented class
 */
class Frame
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $trace;

    /**
     * Undocumented function
     *
     * @param [type] $trace
     */
    public function __construct($trace)
    {

        $this->trace = $trace;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getLine()
    {
        return $this->trace['line'];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getFile()
    {
        return $this->trace['file'];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getClass()
    {
        return $this->trace['class'];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getArgs()
    {
        return $this->trace['args'];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getFunction()
    {
        if(isset($this->trace['function'])) {
            return $this->trace['function'];
        }
        return '[none]';
    }


}

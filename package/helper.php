<?php

/**
 * Return an instance of class register on iod container
 *
 * @param string $name
 * @return void
 */
function get(string $name)
{

    global $iod;
    return $iod[$name];

};

/**
 * Retrieve real class bind to the name
 *
 * @param [type] $name
 * @return void
 */
function get_alias($name)
{

    global $iod;
    return $iod->getAlias($name);

}

/**
 * Undocumented function
 *
 * @param [type] $abstract
 * @param [type] $concrete
 * @param boolean $shared
 * @param boolean $renewable
 * @return void
 */
function bind($abstract, $concrete = null, $shared = false, $renewable = true )
{
    global $iod;
    $iod->bind($abstract, $concrete, $shared, $renewable);
}

/**
 * Undocumented function
 *
 * @param [type] $abstract
 * @param array $parameters
 * @return void
 */
function make($abstract, $parameters = [])
{
    global $iod;
    return $iod->make($abstract, $parameters);
}

/**
 * Create new instance of class bind in iod container
 * !! not already implements
 *
 * @return void
 */
function create()
{

    global $iod;


};

/**
 * bind new class on iod container
 * !! not already implements
 *
 * @return void
 */
function set()
{

    global $iod;

};

/**
 * Set global request
 *
 * @param [type] $newRequest
 * @return void
 */
function setRequest($newRequest)
{

    global $request;
    $request = $newRequest;
    return $request;

};

/**
 * Get global request
 *
 * @return void
 */
function getRequest()
{

    global $request;
    return $request;
    
}

/**
 * Undocumented function
 *
 * @param [type] $way
 * @return void
 */
function url($way)
{

    $path = '';
    if($_SERVER['SERVER_NAME'] != 'localhost' && $_SERVER['SERVER_NAME'] != '127.0.0.1')
    {
        if(secure_url)
            $path = 'https://'.$_SERVER['SERVER_NAME']."/";
        else 
            $path = 'http://'.$_SERVER['SERVER_NAME']."/";
    }
    else
    {
        $path = '/'.app_base_url.'/';
    }

    return $url = $path.$way;


}

/**
 * Undocumented function
 *
 * @param string|null $tag
 * @return void
 */
function sget(?string $tag = null)
{

    sstart();
    if(isset($tag) && isset($_SESSION[$tag]))
        return $_SESSION[$tag];

    return $_SESSION;

}

/**
 * Undocumented function
 *
 * @param string $tag
 * @param [type] $value
 * @return void
 */
function sput(string $tag, $value = null)
{

    sstart();
    $_SESSION[$tag] = $value;

}

/**
 * Undocumented function
 *
 * @param string|null $tag
 * @return void
 */
function shas(?string $tag = null)
{
    if(isset($_SESSION[$tag]) && !empty($_SESSION[$tag]))
        return true;
    
    return false;
}

/**
 * Undocumented function
 *
 * @return void
 */
function sstart(?array $options = [])
{

    if(!sisStart())
        session_start($options);

}

/**
 * Undocumented function
 *
 * @return void
 */
function sisStart()
{
    return session_status() == PHP_SESSION_ACTIVE;
}

/**
 * Undocumented function
 *
 * @return void
 */
function generateToken()
{

    return \bin2hex(\random_bytes(16)) ;

}

/**
 * Undocumented function
 *
 * @return void
 */
function csrf()
{
    echo '<input type="hidden" name="CSRF_TOKEN" value="'.Session::generateToken().'"></input>';
}

/**
 * Undocumented function
 *
 * @param string $token
 * @param string $algo
 * @param string $key
 * @return void
 */
function hash_token(string $token ,string $algo = 'sha256', string $key = '0')
{
    return hash_hmac($algo, $token ,$key);
}
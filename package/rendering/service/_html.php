<?php

namespace App\Rendering\Service;

/**
 * Undocumented class
 */
class _html
{
  
    private $processing = 0;

    private $tags = [
        'meta' => '<meta{{attrs}}/>',
        'metalink' => '<link href="{{url}}"{{attrs}}/>',
        'link' => '<a href="{{url}}"{{attrs}}>{{content}}</a>',
        'mailto' => '<a href="mailto:{{url}}"{{attrs}}>{{content}}</a>',
        'image' => '<img src="{{url}}"{{attrs}}/>',
        'tableheader' => '<th{{attrs}}>{{content}}</th>',
        'tableheaderrow' => '<tr{{attrs}}>{{content}}</tr>',
        'tablecell' => '<td{{attrs}}>{{content}}</td>',
        'tablerow' => '<tr{{attrs}}>{{content}}</tr>',
        'block' => '<div{{attrs}}>{{content}}</div>',
        'blockstart' => '<div{{attrs}}>',
        'blockend' => '</div>',
        'tag' => '<{{tag}}{{attrs}}>{{content}}</{{tag}}>',
        'tagstart' => '<{{tag}}{{attrs}}>',
        'tagend' => '</{{tag}}>',
        'tagselfclosing' => '<{{tag}}{{attrs}}/>',
        'para' => '<p{{attrs}}>{{content}}</p>',
        'parastart' => '<p{{attrs}}>',
        'css' => '<link rel="{{rel}}" href="{{url}}"{{attrs}}/>',
        'style' => '<style{{attrs}}>{{content}}</style>',
        'charset' => '<meta charset="{{charset}}"/>',
        'ul' => '<ul{{attrs}}>{{content}}</ul>',
        'ol' => '<ol{{attrs}}>{{content}}</ol>',
        'li' => '<li{{attrs}}>{{content}}</li>',
        'javascriptblock' => '<script{{attrs}}>{{content}}</script>',
        'javascriptstart' => '<script>',
        'javascriptlink' => '<script src="{{url}}"{{attrs}}></script>',
        'javascriptend' => '</script>',
        'confirmJs' => '{{confirm}}',
    ];

    /**
     * Construct tag with attributs and content
     *
     * @param array $args
     * @return string
     */
    protected function tag(array $args, array $attrs = null)
    {


        $this->process();

        if(!isset($args['template']))
            throw new \InvalidArgumentException(
                "tag function argument must be array containing ['template'] field",
                500
            );

        if(!($path = $this->tags[$args['template']]))
            $path = $this->tags['template'];

        preg_match_all("#{{([a-zA-Z]+)}}#",$path, $matches);
 
        for($i = 0; $i < count($matches[0]); $i++)
        {
            if(($matches[1][$i] != 'content' && $matches[1][$i] != 'attrs')  && !isset($args[$matches[1][$i]]))
                throw new \InvalidArgumentException(
                    $args['template'] . " tag need array containing [ '".$matches[1][$i]."' => value] field",
                    500
                );
            

            if($matches[1][$i] == 'content' && !isset($args[$matches[1][$i]]) || ($matches[1][$i] == 'attrs' && !isset($attrs)))
                $replacement = '';
            else
            {
                if($matches[1][$i] == 'attrs' && isset($attrs))
                {
                    
                    $replacement = ' ';
                    foreach($attrs as $key => $value)
                    {
                        $replacement .= $key.'="'.$value.'" '; 
                    }

                }
                else
                {
                    $replacement = $args[$matches[1][$i]];
                }
            }

            $path = \preg_replace("#".$matches[0][$i]."#", $replacement, $path);

        }

        $this->terminate();

        if($this->processing > 0)
            return $path;
        
        echo $path;

    }

    public function start(string $tag, array $attrs = null)
    {

        $this->process();
        
        $array = ['template' => 'tagstart', 'tag' => $tag];

        $result = $this->tag($array ,$attrs);

        $this->terminate();
       
        if($this->processing > 0)
            return $result;
        echo $result;

    }

    public function end(string $tag)
    {
        $this->process();
        echo $this->tag(['template' => 'tagend', 'tag' => $tag]);
        $this->terminate();
    }

    /**
     * Create link to call css file
     *
     * @param [type] $fileName
     * @return void
     */
    public function css($fileName)
    {
        $this->process();

        $path = project_name."/webroot/css/".$fileName.".css";
        $str = $this->tag([ 'template' => 'css', 'rel' => 'stylesheet preload', 'url' => $path]);
                   
        $this->terminate();
       
        if($this->processing > 0)
            return $str;

        echo $str;    

    }

    /**
     * Return string to call js script
     *
     * @param [type] $filename
     * @param array $options
     * @return void
     */
    public function script($filename, array $options = null)
    {

        $this->process();

        $path = project_name."/webroot/js/".$filename.".js";
        $str = $this->tag([ 'template' => 'javascriptlink', 'rel' => 'preload' , 'url' => $path, 'async' => 'async']);
        
        $this->terminate();
       
        if($this->processing > 0)
            return $str;

        echo $str;    
        
    }

    private function process()
    {
        $this->processing++;
    }

    private function terminate()
    {
        $this->processing--;
    }

    public function __call($name , array $args)
    {

        $this->process();

        $query = array_merge(['template' => $name], $args[0]);
        $result = $this->tag($query, $args[1]);

        $this->terminate();
        return $result;

        echo $result;

    }

}

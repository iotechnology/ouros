<?php

namespace App\Rendering\Service;

/**
 * Undocumented class
 */
class _form
{

    /**
     * Undocumented function
     *
     * @param string $method
     * @param string $action
     * @return void
     */
    public function begin(string $method, string $action)
    {

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function input()
    {

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function submit()
    {

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function end()
    {

    }
    
}

<?php

namespace App\Rendering\Service;


use App\Core\Exception\MissingAction;
use App\Core\Exception\MissingLayout;
use App\Core\Exception\MissingPrefix;
use App\Core\Exception\MissingController;

/**
 * This use is needed to call correct Dictionary in fact after handler exception current path is "/"
 */
use App\Core\Hub\Dictionary;

/**
 * Undocumented class
 */
class _viewmanager
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $data = null;

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $layout = 'Default';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    private $extractInLayout = true;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $prefix;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $controller;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $action;

    /**
     * Undocumented function
     *
     * @param [type] $data
     * @return void
     */
    public function set($data)
    {

        if(!isset($this->data))
            $this->data = [];

        if(isset($data) && !empty($data)) {
            $this->data = array_merge($this->data, $data);
        }

    }

    /**
     * Undocumented function
     *
     * @param [type] $template
     * @return void
     */
    public function render($template = null)
    {

        \ob_start();

        if(isset($this->data) && !empty($this->data) && $this->extractInLayout == true) {
            extract($this->data);
        }

        if(!file_exists(Dictionary::LAYOUT().$this->layout.'.php')) { 
            throw new MissingLayout(Dictionary::LAYOUT().$this->layout.'.php'." layout file not exist in Template/Layout folder");
        }

        require_once Dictionary::LAYOUT().$this->layout.'.php';
        
        $output = ob_get_contents();
       
        \ob_end_clean();

        return $output;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function loadView()
    {

        if(!isset($this->prefix) || empty($this->prefix)) {
            $this->prefix = strtolower(Path::getPrefix());
        }
        if(!isset($this->controller) || empty($this->controller)) {
            $this->controller = strtolower(Path::getController());
        }
        if(!isset($this->action) || empty($this->action)) {
            $this->action = strtolower(Path::getAction());
        }

        $path = Dictionary::VIEW();
        if(isset($this->data) && !empty($this->data)) {
            foreach($this->data as $k => $d){
                ${$k} = $d;
            }
        }
            
        if($this->prefix != 'default') {
            $path = $path.$this->prefix.Dictionary::DIRECTORY();
            $this->prefix = $this->prefix.'/';$this->action;
        }
        else{
            $this->prefix = '';
        }

        if($this->prefix != '' && !is_dir($path)) {
            throw new MissingPrefix($this->prefix." prefix Folder not found in folder Template/View/.", 1);
        }

        $path = $path.strtolower($this->controller);

        if(!is_dir($path)) {
            throw new MissingController($this->controller."Controller Folder not found in folder Template/View/".$this->prefix.".", 1);
        }

        $path = $path.Dictionary::DIRECTORY().$this->action.".php"; 

        if(!file_exists($path)) {
            throw new MissingAction($this->action.".php File not found in folder Template/View/".$this->prefix.strtolower($this->controller).Dictionary::DIRECTORY().".", 0);
        }

        include_once $path;

    }

    /**
     * Undocumented function
     *
     * @param array $url
     * @return void
     */
    public function setView(array $url)
    {

        if(!is_array($url)) { 
            throw new Exception("Call function with not permissible object");
        }
        
        if(isset($url['prefix']) && !empty($url['prefix'])) {
            $this->prefix = $url['prefix'];
        }
        if(isset($url['controller']) && !empty($url['controller'])) {
            $this->controller = $url['controller'];
        }
        if(isset($url['action']) && !empty($url['action'])) {
            $this->action = $url['action'];
        }

    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @return void
     */
    public function getElement($name, ?array $vars = null)
    {
        if(isset($vars) && !empty($vars)) {
            extract($vars);
        }
        include Dictionary::TEMPLATE().'element'.Dictionary::DIRECTORY().$name.'.php';
    }

    /**
     * Undocumented function
     *
     * @param [type] $layout
     * @return void
     */
    public function setLayout($layout = null)
    {
        if(func_num_args() == 0) {
            $this->layout = 'Default';
        }
        
        if(!isset($layout) && is_string($layout)) {
            throw new InvalidArgumentException(
                "Argument to ".__METHOD__."must be string"
            );
        }

        $this->layout = $layout;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @return void
     */
    public function setCss($name)
    {

        include Dictionary::CSS.$name.".css";

    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @return void
     */
    public function setJs($name)
    {

        include Dictionary::JS.$name.".js";

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function extractInLayout()
    {
        $this->extractInLayout = true;
    }

}

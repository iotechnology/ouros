<?php

namespace App\Rendering\Middleware;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Undocumented class
 */
class RenderingMiddleware extends Middleware
{

    /**
     * Undocumented function
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {

        $response = $handler->handle($request);

        $path = $request->getAttribute(uri)['path'];
 
        ViewManager::setView($path);

        $output = ViewManager::render();

        $response->getBody($output)->write($output);

        return $response;

    }

}

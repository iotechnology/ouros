<?php

namespace App\Session\Service;

use App\Session\Exception\NotFoundHandler;
use App\Configuration\Contract\ConfigRegisterInterface;

/**
 * Undocumented class
 */
class _session implements ConfigRegisterInterface
{


    /**
     * Undocumented variable
     *
     * @var array
     */
    private $attributes = [];

    /**
     * Undocumented variable
     *
     * @var array
     */
    private $conf = [
        "session_handler" => "",
        "session" => [
            "use_cookies" => "1",
            "cookie_lifetime" => 0,
            "cache_expire" => 600,
            "gc_maxlifetime" => 10,
            "cookie_httponly" => true,
            "use_strict_mode" => true,
            "cookie_secure" => true,
        ],
        "hash" => "sha256",
        "token_lifetime" => 1800,
    ];

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $sid_function = null;

    /**
     * Undocumented function
     *
     * @param array $option
     */
    public function __construct(string $config_file)
    {

        $this->config($config_file);
        $this->start();
        $this->loadSession();
        
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function loadSession()
    {

        $this->attributes = array_merge($this->attributes, $this->read());
        
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function save()
    {

        $_SESSION = $this->attributes;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function read()
    {

        return $_SESSION;
        
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function start()
    {

        sstart();

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getId()
    {

        return $this->id;

    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @return void
     */
    public function get(string $name)
    {
        
        if($this->has($name))
            return $this->attributes[$name];
        return null;

    }

    /**
     * Undocumented function
     *
     * @param string $name
     * @return boolean
     */
    public function has(string $name)
    {

        if(isset($this->attributes[$name]))
            return true;
        return false;

    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getAttributes()
    {

        return $this->attributes;

    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @param [type] $value
     * @return void
     */
    public function set(string $name, $value)
    {

        $this->attributes[$name] = $value;
    
    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @return void
     */
    public function remove($name)
    {

        if(!$this->has($name))
            throw new Exception(
                $name .' is not in session',
                500
            );
        
        unset($this->attributes[$name]);

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getConfiguration()
    {

        return $conf;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function loadHandler()
    {

        foreach($this->conf["session"] as $k => $v)
        {
            ini_set("session.".$k, $v);
        }

        if(!empty($this->conf["session_handler"]) && $this->conf["session_handler"] != "") {
            
            $handler = new $this->conf["session_handler"]($this->sid_function);
            session_set_save_handler($handler, true);

        }

    }

    /**
     * Undocumented function
     *
     * @param [type] $args
     * @return void
     */
    public function config($args = null)
    {
        if(!isset($args))
            $config = config.config_session;
        else
            $config = config.$args;

        if(!\file_exists($config))
            throw new \RuntimeException(
                "File ".$config." not exist",500
            );
        $result = require_once $config;

        $this->conf =  $result + $this->conf;
        
        $this->loadHandler();

    }

    /**
     * Undocumented function
     *
     * @param [type] $name
     * @param [type] $args
     * @return void
     */
    public function __call($name, $args = null)
    {

        if(isset($args) && !is_null($args) && !empty($args))
            $this->attributes[$name] = $args;
        
        if(shas($name))
            return sget($name);
        return null;

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function token()
    {

        return $this->attributes['_token'];

    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function generateToken()
    {

        if(!$this->has('_token') || !is_array($this->get('_token')))
            $this->set('_token', []);   
        if(count($this->get('_token')) >= 25)
            array_shift($this->attributes['_token']);

        $tokens = $this->get('_token');
        $newToken = generateToken();
        
        $tokens += [hash_token($newToken) => (date('now') + $this->conf['token_lifetime'])];

        $this->set('_token', $tokens);

        return $newToken;

    }

    /**
     * Undocumented function
     *
     * @param [type] $token
     * @return void
     */
    public function removeToken($token)
    {

        if(!$this->has('_token') || !is_array($this->get('_token')))
            throw new \Exception(
                'no token available',
                500
            );

        unset($this->attributes['_token'][$token]);

    }

    public function isValideToken(string $token)
    {
        return date('now') < $this->attributes['_token'][$token];
    }


}

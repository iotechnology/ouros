<?php

use App\Database\Blind\Doctrine;
use Model\Entity\Session;


class DefaultDbSession implements \SessionHandlerInterface
{
    /*
    private $savePath;
    private $entityManager;
    private $func;

    public function __construct($func = null)
    {
        $this->func = $func;
    }

    public function open($save_path, $session_name)
    {

        $this->entityManager = Doctrine::connect();
        return true;

    }

    public function close()
    {

        $this->entityManager->close();
        return $this->entityManager->isOpen();

    }

    public function read($id)
    {

        $session = $this->entityManager->find(Session::class, $id);
        return isset($session) ? htmlspecialchars_decode($session->getData()) : "";

    }

    public function write($id, $data)
    {   

        if(($session = $this->entityManager->find(Session::class, $id)) == null) {
            $session = new Session();
            $session->setId($id);
        }


        $session->setData(htmlspecialchars($data));
        $time = new \DateTime("now");
        $session->setLastAccessTime(clone $time);
        $session->setIp($_SERVER['REMOTE_ADDR']);
        $session->setBrowser($_SERVER['HTTP_USER_AGENT']);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        return true;

    }

    public function destroy($id)
    {

        if(($session = $this->entityManager->find(Session::class, $id)) != null) {
            $this->entityManager->remove($session);
            $this->entityManager->flush();
        }

        return true;
    }

    public function gc($maxlifetime)
    {

        return true;
        
    }

    public function create_sid()
    {
        if(isset($this->func)) {
            return $this->func();
        }
        return hash('sha256', $_SERVER['HTTP_USER_AGENT'].date('now'));
    }
    */
}

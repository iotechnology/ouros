<?php

namespace App\Session\Middleware;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SessionMiddleware extends Middleware
{

    /**
     * process of middleware's traitement
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {
        
        EventManager::trigger('session.before.start', ['request' => $request->getAttributes()]);
        
        $session = make('session', ['config_file' => config_session]);

        EventManager::trigger('session.after.start', ['request' => $request->getAttributes()]);
        
        $response = $handler->handle($request);

        $session->save();

        return $response;
        
    }

}

<?php

namespace App\Routing\Hub;


/**
 * Undocumented class
 */
class Route
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $path;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $url;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $matches;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $params;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $methods;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $options;

    /**
     * Undocumented function
     *
     * @param [type] $url
     * @param [type] $path
     */
    public function __construct($url, $path)
    {

        $this->url = $url;
        $this->path = $path;
        $this->matches = [];
        $this->params = [];
        $this->methods = ['ALL'];

    }

    /**
     * Undocumented function
     *
     * @param [type] $match
     * @return void
     */
    public function placeRegex($match)
    {
        if(isset($this->params[$match[1]])) {
            return '('.$this->params[$match[1]].')';
        }
        return '([^/]+)';
    }

    /**
     * Undocumented function
     *
     * @param [type] $var
     * @param [type] $regex
     * @return void
     */
    public function with($var, $regex)
    {
        $this->params[$var] = str_replace('(', '(?:', $regex);
        return $this;
    }

    /**
     * Undocumented function
     *
     * @param array $methods
     * @return void
     */
    public function setMethods(array $methods)
    {
        $this->methods = $methods;
        return $this;
    }

    /**
     * Undocumented function
     *
     * @param array $methods
     * @return void
     */
    public function setOptions(array $options)
    {
        if(!isset($this->options) || !\is_array($this->options))
            $this->options = [];
        $this->options = array_merge_recursive($options, $this->options);
    }
    
    /**
     * Undocumented function
     *
     * @param [type] $method
     * @return void
     */
    public function findMethod($method)
    {
        return in_array("ALL", $this->methods) || in_array($method, $this->methods);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Undocumented function
     *
     * @param [type] $path
     * @return void
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Undocumented function
     *
     * @param [type] $matches
     * @return void
     */
    public function setMatches($matches)
    {
        $this->matches = $matches;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getParams()
    {
        return $this->params;
    }
   
    /**
     * Undocumented function
     *
     * @return void
     */
    public function toArray()
    {

        return [
            'url' => $this->url,
            'path' => $this->path,
            'matches' => $this->matches,
            'params' => $this->params,
            'options' => $this->options,
            'methods' => $this->methods,
        ];
    }

}

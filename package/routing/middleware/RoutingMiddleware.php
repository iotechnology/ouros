<?php

namespace App\Routing\Middleware;

use \GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class RoutingMiddleware extends Middleware
{

    /**
     * process of middleware's traitement
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return void
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler)
    {
        
        EventManager::trigger('route.before.parse', ['request' => $request->getAttributes()]);

        $parser = make('routeParser');
        $route = $parser->parse($request);

        $request = $request->withAttribute(uri, $route->toArray());
        $response = $this->detachMiddleware($route, $request, $handler);
        if($response instanceof Response)
            return $response;
        if($response instanceof ServerRequestInterface)
            $request = $response;

        EventManager::trigger('route.after.parse', ['request' => $request->getAttributes()]);
        
        $response = $handler->handle($request);
        return $response;
        
    }

    
    private function detachMiddleware($route, $request, $handler)
    {

        if(isset($route->toArray()['options']) && isset($route->toArray()['options']['middleware']))
        {
            if(\is_array($route->toArray()['options']['middleware']))
            {
                foreach($route->toArray()['options']['middleware'] as $middleware){
                    $response = $handler->handleDetach($middleware, $request);
                    if($response instanceof Response)
                        return $response;
                    if($response instanceof ServerRequestInterface)
                        $resquest = $response;
                }

            }
            if(\is_string($route->toArray()['options']['middleware']))
                $response = $handler->handleDetach($route->toArray()['options']['middleware'], $request);
        }
        if(isset($response))
            return $response;

    }

}

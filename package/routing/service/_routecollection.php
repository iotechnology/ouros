<?php

namespace App\Routing\Service;

/**
 * Undocumented class
 */
class _routecollection
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $routes;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $tmpPrefix;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $tmpMethods;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $tmpOptions;

    /**
     * Undocumented function
     */
    public function __construct()
    {
        $this->routes = [];
        $this->tmpPrefix = 'default';
    }

    /**
     * Undocumented function
     *
     * @param array $route
     * @param [type] $options
     * @return void
     */
    public function addRoute(array $route, $options = null)
    {

        $route[key($route)]['prefix'] = $this->tmpPrefix;
        $url = key($route);

        if($route[key($route)]['prefix'] != 'default') {
            if(key($route) != '')
                $url = $route[key($route)]['prefix'].'/'.$url;
            else
                $url = $route[key($route)]['prefix'];
        }
        
        $newRoute = new Route($url, $route[key($route)]);
        if(isset($options)) {
            foreach($options as $k => $v){
                $newRoute->with($k, $v);
            }
        }

        $this->routes[$this->tmpPrefix][] = $newRoute;
        
        if(!is_null($this->tmpOptions) && !empty($this->tmpOptions))
            $newRoute->setOptions($this->tmpOptions);

        return $newRoute;
        
    }

    /**
     * Undocumented function
     *
     * @param string $prefix
     * @return void
     */
    public function getCollection(string $prefix)
    {

        return $this->routes[$prefix];
    }

    /**
     * Undocumented function
     *
     * @param array $routes
     * @return void
     */
    public function addRoutes(array $routes)
    {

        foreach($routes as $route){
            $this->addRoute($route[0], $route[1], $route[2]);
        }

    }

    /**
     * Undocumented function
     *
     * @param string $prefix
     * @param callable $callable
     * @return void
     */
    public function scope(string $prefix, callable $callable, array $options)
    {
        $this->tmpOptions = $options;
        $this->tmpPrefix = $prefix;
        call_user_func($callable);
        $this->tmpPrefix = 'default';
        $this->tmpOptions = [];
    }

    /**
     * Undocumented function
     *
     * @param [type] $url
     * @return void
     */
    public function matchPrefix($url)
    {
        
        $tmp = explode('/', $url);
        $prefix = $tmp[0];
        unset($tmp[$prefix]);
        $tmp = implode('/', $tmp);
        return key_exists($prefix, $this->routes) ? [$tmp, $prefix] : [$url, 'default'];

    }

}

<?php

namespace App\Routing\Service;


/**
 * Undocumented class
 */
class _routeparser
{

    /**
     * Undocumented function
     *
     * @param [type] $request
     * @return void
     */
    public function parse($request)
    {
        if(isset($request->getAttributes()[url])) {
            $url = $request->getAttributes()[url];
        } else {
            $url = "/";
        }
        $sm = $request->getserverParams()['REQUEST_METHOD'];

        $path = Routes::matchPrefix($url);
        $routes = Routes::getCollection($path[1]);

        foreach($routes as $route){

            if($route->findMethod($sm) && $this->match($path[0], $route)) {
                return $route;
            }

        }

        throw new \Exception('No matching routes !', 404);

    }

    /**
     * Undocumented function
     *
     * @param [type] $url
     * @param Route $route
     * @return void
     */
    public function match($url, Route $route)
    {

        $url = trim($url, '/');
        $path = preg_replace_callback('#(:[\w]+)#', [$route, 'placeRegex'], $route->getUrl());

        $path = preg_replace('#/([\*])#', '([\w/]+)', $path);
        $regex = "#^$path$#";

        if(!preg_match($regex, $url, $matches)) {     
                return false;
        }else{

            if(preg_match('#/([\*])#', $route->getUrl())) {

                array_shift($matches);
                $regex2 = "#([\w]+)#";
                preg_match_all($regex2, $matches[0], $matches2);
                $matches = $matches2[1];

                if(!isset($route->getPath()['action'])) {

                    $path = $route->getPath();  
                    $path['action'] = $matches[0];
                    $route->setPath($path);

                }  
            }

        }

        array_shift($matches);
        if(!empty($matches)) {      
            $route->setMatches($matches);
        }

        return true;

    }

    /**
     * Return error route
     * !!! This route is generate automaticaly and is not storage with other route
     * !!! it's use like this to restric access of controller and view when is not neccessary
     * @return void
     */
    public function getErrorRoute()
    {

        return new Route('error/debug/public', ['controller' => 'Debug' , 'action' => 'public', 'prefix' => 'error' ]);

    }

}

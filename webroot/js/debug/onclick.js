function Onclicky(index){
    var elements = document.getElementsByClassName("block-info");
    for(var el of elements){
        if(el.classList.contains("active")){
            el.classList.remove("active"); 
        }
        if(!el.classList.contains("active") && el.classList.contains(index)){
            el.classList.add("active"); 
        }
    }
    var els = document.getElementsByClassName("trace");
    for(var el of els){
        if(el.classList.contains("active")){
            el.classList.remove("active"); 
        }
        if(!el.classList.contains("active") && el.classList.contains(index)){
            el.classList.add("active"); 
        }
    }
}
<?php

/**
 * Autoconfigure using file containing .inc.php
 */
Configure::configure();

/**
 * Configure Dispatcher
 */
Configure::dispatcher();

/**
 * Configure Debugger Manager
 */
Configure::debug();

/**
 * Configure Csrf protection
 */
Configure::csrfMiddleware();

/**
 * Configure the access of database
 */
Configure::access();

/**
 * Configure Auth
 */
Configure::authentication();
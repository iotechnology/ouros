<?php

return [

    'middlewares' => [

        'App\\Debug\\Middleware\\SupervisorMiddleware',
        'App\\Session\\Middleware\\SessionMiddleware',
        'App\\Routing\\Middleware\\RoutingMiddleware',
        'App\\Rendering\\Middleware\\RenderingMiddleware',
        'App\\Controller\\Middleware\\ControllerMiddleware',
        'App\\Security\\Middleware\\CsrfMiddleware',

        
    ],

    'detach_middlewares' => [
        
        'auth'              => 'App\Authentication\Middleware\AuthenticateMiddleware',
        
    ],

];
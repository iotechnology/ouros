<?php

return [

    'dispatcher'                        => ['App\Core\Service\_dispatcher', true, false],
    'configure'                         => ['App\Configuration\Service\_configure', true, false],
    'eventManager'                      => ['App\Event\Service\_eventmanager', true, false],
    'access'                            => ['App\Database\Service\_access', true, false],
    'debug'                             => ['App\Debug\Service\_debugmanager', true, false],
    'managerFactory'                    => 'App\Factory\Factories\ManagerFactory',
    'form'                              => ['App\Rendering\Service\_form', true, false],
    'html'                              => ['App\Rendering\Service\_html', true, false],
    'viewManager'                       => ['App\Rendering\Service\_viewmanager', true, false],
    'routes'                            => ['App\Routing\Service\_routecollection', true, false],
    'routeParser'                       => 'App\Routing\Service\_routeparser',
    'session'                           => ['App\Session\Service\_session', true, false],
    'auth'                              => ['App\Authentication\Middleware\AuthenticateMiddleware', true, false],
    'authentication'                    => ['App\Authentication\Service\_authentication', true, false],
    'csrfMiddleware'                    => ['App\Security\Middleware\CsrfMiddleware', true, false],

];
<?php

return [

    'default' => [

        'options' => [
            'path' => [
                'src/model/entity/',
            ],
            'devMode' => true,
            'proxyDir' => null,
            'cache' => null,
            'useSimpleAnnotationReader' => false,
            
        ],
        
        'dbparams' => [
            'driver' => 'pdo_mysql',
            'user' => 'root',
            'password' => '',
            'dbname' => 'ouros',
        ],
    ],


];
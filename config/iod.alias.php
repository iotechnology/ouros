<?php

return [

    /**
     * Core
     */
    'Dispatcher'                        => 'App\Core\Blind\Dispatcher',
    'Blind'                             => 'App\Core\Hub\Blind',
    'Container'                         => 'App\Core\Hub\Container',
    'Dictionary'                        => 'App\Core\Hub\Dictionary',
    
    /**
    * Configuration
    */
    'Configure'                         => 'App\Configuration\Blind\Configure',

    /**
     * Event
     */
    'EventManager'                      => 'App\Event\Blind\EventManager',
    'ListenerProvider'                  => 'App\Event\Hub\ListenerProvider',
    'Listener'                          => 'App\Event\Hub\Listener',
    'Event'                             => 'App\Event\Hub\Event',
    
    /**
     * Controller
     */
    'Controller'                        => 'App\Controller\Hub\Controller',
    'ControllerLoader'                  => 'App\Controller\Hub\ControllerLoader',
    
    /**
     * Database
     */
    'Access'                            => 'App\Database\Blind\Access',

    /**
     * Debug
     */
    'Debug'                             => 'App\Debug\Blind\DebugManager',

    /**
     * Factory
     */
    'Factory'                           => 'App\Factory\Hub\Factory',
    'ManagerFactory'                    => 'App\Factory\Factories\ManagerFactory',

    /**
     * Manager
     */
    'Manager'                           => 'App\Management\Hub\HandlerManager',
    'MiddlewareManager'                 => 'App\Management\Hub\MiddlewareManager',
    'CallbackHandler'                   => 'App\Management\Handler\CallbackHandler',
    'Middleware'                        => 'App\Management\Hub\Middleware',


    /**
     * Rendering
     */
    'Form'                              => 'App\Rendering\Blind\Form',
    'Html'                              => 'App\Rendering\Blind\Html',
    'ViewManager'                       => 'App\Rendering\Blind\ViewManager',

    /**
     * Routing
     */
    'Route'                             => 'App\Routing\Hub\Route',
    'Routes'                            => 'App\Routing\Blind\RouteCollection',
    'RouteParser'                       => 'App\Routing\Blind\RouteParser',

    /**
     * Security
     */
    'CsrfToken'                         => 'App\Security\Blind\CsrfToken',

    /**
     * Authentication
     */
    'Authentication'                    => 'App\Authentication\Blind\Authentication',
    'Token'                             => 'App\Authentication\Hub\Token',
    'Password'                          => 'App\Authentication\Hub\Password',
    
    /**
     * Session
     */
    'Session'                           => 'App\Session\Blind\Session',


    /**
     * Exceptions
     */

    'NotExistClassEception'             => 'App\Core\Exception\NotExistClassException',
    'NotFoundException'                 => 'App\COre\Exception\NotFOundException',
    'MissingAction'                     => 'App\Controller\Exception\MissingAction',
    'MissingController'                 => 'App\Controller\Exception\MissingController',
    'MissingFunction'                   => 'App\Controller\Exception\MissingFunction',
    'MissingPrefix'                     => 'App\Controller\Exception\MissingPrefix',

    /**
     * Utility
     */
    'FileSystem'                         => 'App\Utility\FileSystem\Hub\FileSystem',


];
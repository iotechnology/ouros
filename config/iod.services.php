<?php

return [

    'dispatcher',
    'configure',
    'eventManager',
    'access',
    'debug',
    'managerFactory',
    'form',
    'html',
    'viewManager',
    'routes',
    'authentication',

];
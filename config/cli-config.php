<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

// bootstrap.php
require_once "vendor/autoload.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array(dirname(__DIR__)."/src/model/entity/");
$isDevMode = false;

// the connection configuration
$dbParams = require_once "config.database.php";

$dbParams = $dbParams['default']['dbparams'];

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);

$entityManager = EntityManager::create($dbParams, $config);

return ConsoleRunner::createHelperSet($entityManager);
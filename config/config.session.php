<?php

return [

    "session" => [
        "use_cookies" => "1",
        "cookie_lifetime" => 0,
        "cache_expire" => 600,
        "gc_maxlifetime" => 10,
        "cookie_httponly" => true,
        "use_strict_mode" => true,
        "cookie_secure" => true,
    ],
    "session_handler" => "",
    "hash" => "sha256",
    "token_lifetime" => 1800,
    
];
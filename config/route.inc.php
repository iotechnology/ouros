<?php

Routes::scope('default', function (){
    Routes::addRoute([
        '' => ['controller' => 'Main', 'action' => 'index'],
    ]);

    Routes::addRoute([
        '/*' => ['controller' => 'Main'],     
    ])->setMethods(['GET','POST']);

}, []);


Routes::scope('admin', function (){
    Routes::addRoute([
        '' => ['controller' => 'Main', 'action' => 'dashboard'],
    ]);

    Routes::addRoute([
        '/*' => ['controller' => 'Main'],
    ])->setMethods(['GET','POST']);
}, ['middleware' => 'auth', 'gate' => 'admin']);

Routes::scope('error', function () {

    Routes::addRoute([
        'public' => ['controller' => 'Debug', 'action' => 'public'],
    ]);

},[]);
<?php

return [

    'gates' => [

        'user' => [
            'token'     => true,
            'scope'     => '/', // use to restricted access of data at the specific way
            'driver'    => 'form',  
            'model'     => 'Model\Authentication\User',

        ],

        'admin' => [
            'token'     => true,
            'driver'    => 'form',  
            'model'     => 'Model\Authentication\User',
            'except'    => [
                
            ],
        ],

        'basic' => [
            'token'     => true,
            'realm'     => 'MyRealm',
            'driver'    => 'basic',
            'model'     => 'Model\Authentication\User',
        ],

    ],

];
<!DOCTYPE html>

<html>
    <head>
        <title>Catch Exception</title>
        <?php echo Html::css("debug/default"); ?>
        <?php echo Html::css("debug/prism"); ?>
    </head>
    <body>
        <?php $this->loadView();?>
        <?php echo Html::script("debug/onclick"); ?>
        <?php echo Html::script("debug/prism"); ?>
    </body>
</html> 
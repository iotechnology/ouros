<?php use App\Debug\Blind\ErrorManager; ?>
<div class="exception-header">
    <h2>
        Exception Catch :
    </h2>
    <?php
        $class =  explode('\\', get_class($exception));
        $last = array_pop($class);
        foreach($class as $c):
        
    ?>
            <h5><?= $c ?> \</h5>
    <?php
        endforeach;
    ?>
    <h5 class="exception-nclass"><?= $last ?></h5>
    
    <h3>
    <?php
        echo $exception->getMessage();
        
    ?>
    </h3>
</div>
<div class="exception-code-block">
        <p><?php echo $exception->getCode();?></p>
</div>
<p class="trace-count">Stack traces : <?php echo count($frames); ?></p>

<div class="body-exception">
    <div class="trace-stack">
        <?php
        $index = count($frames)-1;
        foreach( $frames as $frame): ?>
            <div class="trace <?php echo $index;?>" onclick="Onclicky(<?php echo $index ;?>)">
                <p class="trace-index"><?= $index ?></p> <p class="trace-class"> <?= $frame->getClass() ?> </p>
                        <div class="trace-line">
                            <p >line : </p><p class="line-n"> <?= $frame->getLine() ?></p> 
                        </div>
                        <p class="trace-file"> <?= $frame->getFile() ?> </p>
            </div>
        <?php
        $index--; 
        endforeach;
        ?>
    </div>

        <?php
        $index = count($frames) - 1;
        foreach($frames as $frame): ?>
            <div class='block-info language-php <?php echo "$index";?>'>
                        <div class="block-info-header">
                            <p class="trace-file"> <?= $frame->getFile() ?> </p>                            
                            <div class="trace-function">
                                <p >function : </p><p class="function"> <?= $frame->getFunction() ?> </p>
                            </div>
                            <div class="trace-line">
                                <p >line : </p><p class="line-n"><?= $frame->getLine() ?></p> 
                            </div>
                        </div>
                <div class="container-block-info-code">
                    <table class="block-info-code <?php $index ?>">
                    <?php
                            if($frame->getFile() != '[internal]'):
                                $file_lines = file($frame->getFile());
                                $linenumber = 0;
                                foreach ($file_lines as $line):
                                    $linenumber++;
                                    if($linenumber < $frame->getLine() + 9 && $linenumber > $frame->getLine() - 9 && $linenumber != $frame->getLine()): ?>
                                <tr class="line-block"><td class="line-number-code"><code><?php echo $linenumber."." ?></code></td><td class="line-code"><code><?php echo($line) ?></code></td></tr>
                                    <?php
                                    endif;
                                    if($linenumber == $frame->getLine()) :?>
                                <tr class="line-block current-line-code"><td class="line-number-code"><code><?php echo $linenumber."." ?></code></td><td class="line-code"><code><?php echo($line) ?></code></td></tr>
                                    <?php
                                    endif;
                                    if($linenumber > $frame->getLine() + 9) :
                                        break;
                                    endif; 

                                endforeach;
                            else: ?>

                                <p class="unknow-file"> <?php echo "<strong>Unknown File :</strong> because function <strong>\"".$frame->getFunction()."\"</strong> has call with reflection method."; ?></p>
 
                           <?php endif;?>
                    </table>
                </div>
                <?php $index--; ?>
            </div>
        <?php
        endforeach;
        ?>
</div>

<div class="block-info-environement">
    <div class="block-info-environement-header">
        <h2>Environement</h2> <h2 class="orange">&</h2> <h2>details</h2>
    </div>
    <?php $that->printEnv($GLOBALS);?>
    
</div>






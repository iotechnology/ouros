<?php

namespace Model\Authentication;

use App\Authentication\Contract\AuthenticableInterface;

class User implements AuthenticableInterface
{

    public function registerNewToken($user, $token)
    {

        $user->setAuthToken($token);
        $em = Access::get();
        $em->persist($user);
        $em->flush();

    }

    public function getById($id)
    {
        $em = Access::get();
        $rep = $em->getRepository('Model\Entity\User');
        return $rep->findOneBy(['idUser' => $id]);
    }

    public function getByToken($token)
    {
        $em = Access::get();
        $rep = $em->getRepository('Model\Entity\User');
        return $rep->findOneBy(['authToken' => $token]);
    }
    
    public function getByCredentials(array $credentials)
    {

        $em = Access::get();
        $rep = $em->getRepository('Model\Entity\User');
        if(isset($credentials['password']))
        {
            $password = new Password($credentials['password']);
            $credentials['password'] = $password->hash();
        }
        return $rep->findOneBy($credentials);

    }



}
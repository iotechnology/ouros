<?php

namespace Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="session")
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\Column(name="session_id",type="string", length=255, nullable=false)
     */
    protected $id;

    /**
     * @ORM\Column(name="session_data", type="text")
     */
    protected $data;

    /**
     * @ORM\Column(name="session_ip", type="string", length=15, nullable=false)
     */
    protected $ip;
    /**
     * @ORM\Column(name="session_lastaccesstime", type="datetime", nullable=false)
     */
    protected $lastAccessTime;

    /**
     * @ORM\Column(name="session_browser", type="string", length=255, nullable=false)
     */
    protected $browser;


    // getters et setters

    public function getId(){
        return $this->id;
    }

    public function getData(){
        return $this->data;
    }

    public function getIp(){
        return $this->ip;
    }

    public function getLastAccessTime(){
        return $this->lastAccessTime;
    }

    public function getBrowser(){
        return $this->browser;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function setData($data){
        $this->data = $data;
    }

    public function setIp($ip){
        $this->ip = $ip;
    }

    public function setLastAccessTime($time){
        $this->lastAccessTime = $time;
    }

    public function setBrowser($browser){
        $this->browser = $browser;
    }

}
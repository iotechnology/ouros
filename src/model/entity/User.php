<?php

namespace Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Model\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $idUser;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $login;


    /**
     * @ORM\Column(type="string", length=80)
     */
    protected $password;

    /**
     * @ORM\Column(name="last_conn", type="datetime", nullable=true)
     */
    protected $lastConnection;

    /**
     * @ORM\Column(name="auth_token", type="string", length=255, nullable=true)
     */
    protected $authToken;

    // getters et setters

    public function getIdUser(){
        return $this->idUser;
    }

    public function getlogin(){
        return $this->login;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getLastConnection(){
        return $this->lastConnection;
    }

    public function getAuthToken(){
        return $this->authToken;
    }

    public function setIdUser($id){
        $this->idUser = $id;
    }

    public function setLogin($login){
        $this->login = $login;
    }

    public function setPassword($pswd){
        $this->password = $pswd;
    }

    public function setLastConnection($lc){
        $this->lastConnection = $lc;
    }

    public function setAuthToken($at){
        $this->authToken = $at;
    }
    
}
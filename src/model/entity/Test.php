<?php

namespace Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Model\Repository\TestRepository")
 * @ORM\Table(name="test")
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $idTest;

    /**
     * @ORM\Column(type="string", length=45)
     */
    protected $name;

    // getters et setters

    public function getIdTest(){
        return $this->idTest;
    }

    public function getName(){
        return $this->name;
    }
    public function setIdTest($id){
        $this->idTest = $id;
    }

    public function setName($name){
        $this->name = $name;
    }
}
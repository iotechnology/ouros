<?php

namespace Model\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository{


    public function findUser(string $name){

        $user = $this->findOneBy(['login' => $name]);
        return $user;

    }


}